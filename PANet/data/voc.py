# -*- coding: utf-8 -*-
'''
# Created on 12月-30-20 16:22
# @filename: voc.py
# @author: tcxia
'''

import os
import numpy as np
from PIL import Image

import torch
import torch.utils.data as tud

class VOC(tud.Dataset):
    def __init__(self, root_dir, split, transforms=None, to_tensor=None) -> None:
        super().__init__()
        self._image_dir = os.path.join(root_dir, 'JPEGImages')
        self._label_dir = os.path.join(root_dir, 'SegmentationClass')
        self._instance_dir = os.path.join(root_dir, 'SegmentationObject')
        self._id_dir = os.path.join(root_dir, 'ImageSets', 'Segmentation')
        
        self.transforms = transforms
        self.to_tensor = to_tensor

        self.split = split
        
        with open(os.path.join(self._id_dir, f'{self.split}.txt'), 'r') as f:
            self.ids = f.read().splitlines()

        self.aux_attrib = {}
        self.aux_attrib_args = {}

    def __len__(self) -> int:
        return len(self.ids)

    def __getitem__(self, index):
        _id = self.ids[index]
        image = Image.open(os.path.join(self._image_dir, f'{_id}.jpg'))
        semantic_mask = Image.open(os.path.join(self._label_dir, f'{_id}.png'))
        instance_mask = Image.open(os.path.join(self._instance_dir, f'{_id}.png'))

        sample = {
            'image': image,
            'label': semantic_mask,
            'instance': instance_mask
        }

        if self.transforms is not None:
            sample = self.transforms(sample)

        image_t = torch.from_numpy(np.array(sample['image']).transpose(2, 0, 1))
        if self.to_tensor is not None:
            sample = self.to_tensor(sample)

        sample['id'] = _id
        sample['image_t'] = image_t

        return sample

    def add_attrib(self, key, func, func_args):
        if key in self.aux_attrib:
            raise KeyError("Attribute '0' already exists, please use 'set_attrib'".format(key))
        else:
            self.set_attrib(key, func, func_args)
    
    def set_attrib(self, key, func, func_args):
        self.aux_attrib[key] = func
        self.aux_attrib_args[key] = func_args

    def subsets(self, sub_ids, sub_args_lst=None):
        indices = [[self.ids.index(id_) for id_ in ids] for ids in sub_ids]
        if sub_args_lst is not None:
            subsets = []


class Subset(tud.Dataset):
    def __init__(self, dataset, indices, sub_attrib_args) -> None:
        super().__init__()
