# -*- coding: utf-8 -*-
'''
# Created on 12月-30-20 17:47
# @filename: voc_custom.py
# @author: tcxia
'''
import os
from data.voc import VOC

def voc_fewshot(root_dir, split, transforms, to_tensor, labels, n_ways, n_shots, max_iters, n_queries=1):
    voc = VOC(root_dir, split, transforms, to_tensor)
    
    sub_ids = []
    for label in labels:
        with open(os.path.join(voc._id_dir, voc.split, 'class{}.txt'.format(label)), 'r') as f:
            sub_ids.append(f.read().splitlines())

    