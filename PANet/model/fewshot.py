# -*- coding: utf-8 -*-
'''
# Created on 12月-30-20 13:20
# @filename: fewshot.py
# @author: tcxia
'''

import torch
import torch.nn as nn

from collections import OrderedDict

from model.vgg import Encoder

class FewShotSeg(nn.Module):
    def __init__(self, in_channels=3, pretrained_path=None, cfg=None) -> None:
        super().__init__()
        self.pretrained_path = pretrained_path
        self.config = cfg or {'align': False}

        self.encoder = nn.Sequential(OrderedDict([('backbone', Encoder(in_channels, self.pretrained_path))]))

    
    def forward(self, supp_imgs, fore_mask, back_mask, qry_imgs):
        n_ways = len(supp_imgs)
        n_shots = len(supp_imgs[0])
        n_queries = len(qry_imgs)

        batch_size = supp_imgs[0][0].shape[0]
        img_size = supp_imgs[0][0].shape[-2:]
        
