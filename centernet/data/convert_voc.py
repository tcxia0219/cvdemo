# -*- coding: utf-8 -*-
'''
# Created on 2021/01/12 16:22:15
# @filename: convert_voc.py
# @author: tcxia
'''

import xml.etree.ElementTree as ET
import os

classes = ['songgu', 'yiwu', 'duangu', 'sunshang', 'zhaxiansongsan', 'xiushi']

def convert_anno(anno_file, res_file):
    in_file = open(anno_file, encoding='utf-8')
    tree = ET.parse(in_file)
    root = tree.getroot()

    for obj in root.iter('object'):
        if obj.find('code') is None:
            cls_name = obj.find('name').text
        else:
            cls_name = obj.find('code').text

        if cls_name not in classes:
            continue
        cls_id = classes.index(cls_name)
        xmlbox = obj.find('bndbox')
        b = (int(xmlbox.find('xmin').text), int(xmlbox.find('ymin').text), int(xmlbox.find('xmax').text), int(xmlbox.find('ymax').text))
        res_file.write(" " + ",".join([str(a) for a in b]) + ',' + str(cls_id))

if __name__ == "__main__":
    anno_file = '/data/line/voc/yolo.data'
    res_dir = '/data/line/voc/centernet.data'
    img_dir = '/data/line/voc/JPEGImages'
    with open(anno_file, 'r') as f_in:
        cont = f_in.read().strip().split()

    file = open('/data/line/voc/centernet.data', 'w')

    for anno_id in cont:
        txt_name = os.path.splitext(os.path.split(anno_id)[1])[0]
        img_file = os.path.join(img_dir, txt_name + '.jpg')

        file.write(img_file)
        convert_anno(anno_id, file)
        file.write('\n')
    file.close()
