# -*- coding: utf-8 -*-
'''
# Created on 2021/01/27 16:14:46
# @filename: DLAnet.py
# @author: tcxia
'''

import math
import numpy as np
import os
import torch
import torch.nn as nn


def get_model_url(data='imagenet', name='dla34', hashid='ba72cf86'):
    return os.path.join('http://dl.yf.io/dla/models', data, '{}-{}.pth'.format(name, hashid))


def conv3x3(in_planes, out_planes, stride=1):
    return nn.Conv2d(in_planes, out_planes, kernel_size=3, stride=stride, padding=1, bias=False)

class BasicBlock(nn.Module):
    def __init__(self, in_planes, planes, stride=1, dilation=1) -> None:
        super().__init__()

        self.conv1 = nn.Conv2d(in_planes,
                               planes,
                               kernel_size=3,
                               stride=stride,
                               padding=dilation,
                               bias=False,
                               dilation=dilation)
        self.bn1 = nn.BatchNorm2d(planes)
        self.relu = nn.ReLU(inplace=True)

        self.conv2 = nn.Conv2d(planes,
                               planes,
                               kernel_size=3,
                               stride=1,
                               padding=dilation,
                               bias=False,
                               dilation=dilation)
        self.bn2 = nn.BatchNorm2d(planes)
        self.stride = stride

    def forward(self, x, residual=None):
        if residual is None:
            residual = x

        out = self.conv1(x)
        out = self.bn1(out)
        out = self.relu(out)

        out = self.conv2(out)
        out = self.bn2(out)

        out += residual
        out = self.relu(out)
        return out

class Bottleneck(nn.Module):
    expansion = 2

    def __init__(self, in_planes, planes, stride=1, dilation=1) -> None:
        super().__init__()

        bottle_planes = planes // self.expansion

        self.conv1 = nn.Conv2d(in_planes, bottle_planes, kernel_size=1, bias=False)
        self.bn1 = nn.BatchNorm2d(bottle_planes)

        self.conv2 = nn.Conv2d(bottle_planes,
                               bottle_planes,
                               kernel_size=3,
                               stride=stride,
                               padding=dilation,
                               bias=False,
                               dilation=dilation)
        self.bn2 = nn.BatchNorm2d(bottle_planes)

        self.conv3 = nn.Conv2d(bottle_planes, planes, kernel_size=1, bias=False)
        self.bn3 = nn.BatchNorm2d(bottle_planes)
        self.relu = nn.ReLU(inplace=True)

        self.stride = stride

    def forward(self, x, residual=None):
        if residual is not None:
            residual = x

        out = self.conv1(x)
        out = self.bn1(out)
        out = self.relu(out)

        out = self.conv2(out)
        out = self.bn2(out)
        out = self.relu(out)

        out = self.conv3(out)
        out = self.bn3(out)

        out += residual
        out = self.relu(out)
        return out

class BottlenckX(nn.Module):
    expansion = 2
    cardinality = 32

    def __init__(self, in_planes, planes, stride=1, dilation=1) -> None:
        super().__init__()

        bottle_planes = planes * self.cardinality // 32
        self.conv1 = nn.Conv2d(in_planes, bottle_planes, kernel_size=1, bias=False)
        self.bn1 = nn.BatchNorm2d(bottle_planes)

        self.conv2 = nn.Conv2d(bottle_planes,
                               bottle_planes,
                               kernel_size=3,
                               stride=stride,
                               padding=dilation,
                               bias=False,
                               dilation=dilation,
                               groups=self.cardinality)
        self.bn2 = nn.BatchNorm2d(bottle_planes)

        self.conv3 = nn.Conv2d(bottle_planes, planes, kernel_size=1, bias=False)
        self.bn3 = nn.BatchNorm2d(planes)

        self.relu = nn.ReLU(inplace=True)
        self.stride = stride

    def forward(self, x, residual=None):
        if residual is not None:
            residual = x

        out = self.conv1(x)
        out = self.bn1(out)
        out = self.relu(out)

        out = self.conv2(out)
        out = self.bn2(out)
        out = self.relu(out)

        out = self.conv3(out)
        out = self.bn3(out)

        out += residual
        out = self.relu(out)
        return out

class Root(nn.Module):
    def __init__(self, inc, outc, kernel_size, residual) -> None:
        super().__init__()
        self.conv = nn.Conv2d(inc, outc, kernel_size=1, stride=1, bias=False, padding=(kernel_size - 1) // 2)
        self.bn = nn.BatchNorm2d(outc)
        self.relu = nn.ReLU(inplace=True)
        self.residual = residual

    def forward(self, *x):
        children = x
        x = self.conv(torch.cat(x, 1))
        x = self.bn(x)
        if self.residual:
            x += children[0]
        x = self.relu(x)
        return x

class Tree(nn.Module):
    def __init__(self,
                 levels,
                 block,
                 inc,
                 outc,
                 stride=1,
                 level_root=False,
                 root_dim=0,
                 root_kernal_size=1,
                 dilation=1,
                 root_residual=False) -> None:
        super().__init__()

        if root_dim == 0:
            root_dim = 2 * outc
        if level_root:
            root_dim += inc
        if levels == 1:
            self.tree1 = block(inc, outc, stride, dilation=dilation)
            self.tree2 = block(outc, outc, 1, dilation=dilation)

            self.root = Root(root_dim, outc, root_kernal_size, root_residual)
        else:
            self.tree1 = Tree(levels - 1,
                              block,
                              inc,
                              outc,
                              stride,
                              root_dim=0,
                              root_kernal_size=root_kernal_size,
                              dilation=dilation,
                              root_residual=root_residual)
            self.tree2 = Tree(levels - 1,
                              block,
                              outc,
                              outc,
                              stride,
                              root_dim=root_dim + outc,
                              root_kernal_size=root_kernal_size,
                              dilation=dilation,
                              root_residual=root_residual)

        self.level_root = level_root
        self.root_dim = root_dim
        self.downsample = None
        self.project = None
        self.levels = levels

        if stride > 1:
            self.downsample = nn.MaxPool2d(stride, stride=stride)

        if inc != outc:
            self.project = nn.Sequential(
                nn.Conv2d(inc, outc, kernel_size=1, stride=1, bias=False),
                nn.BatchNorm2d(outc)
            )

    def forward(self, x, residual=None, children=None):
        children = [] if children is None else children
        bottom = self.downsample(x) if self.downsample else x
        residual = self.project(bottom) if self.project else bottom
        if self.level_root:
            children.append(bottom)
        x1 = self.tree1(x, residual)
        if self.levels == 1:
            x2 = self.tree2(x1)
            x = self.root(x2, x1, *children)
        else:
            children.append(x1)
            x = self.tree2(x1, children=children)
        return x

class DLA(nn.Module):
    def __init__(self,
                 levels,
                 channels,
                 num_classes=1000,
                 block=BasicBlock,
                 residual_root=False,
                 return_levels=False,
                 pool_size=7,
                 linear_root=False) -> None:
        super().__init__()
        self.channels = channels
        self.return_levels = return_levels
        self.num_classes = num_classes
        self.base_layer = nn.Sequential(
            nn.Conv2d(3, channels[0], kernel_size=7, stride=1, padding=3, bias=False),
            nn.BatchNorm2d(channels[0]),
            nn.ReLU(inplace=True)
        )

        self.level0 = self._make_conv_level(channels[0], channels[0], levels[0])
        self.level1 = self._make_conv_level(channels[0], channels[1], levels[1], stride=2)

        self.level2 = Tree(levels[2], block, channels[1], channels[2], 2, level_root=False, root_residual=residual_root)
        self.level3 = Tree(levels[3], block, channels[2], channels[3], 2, level_root=True, root_residual=residual_root)
        self.level4 = Tree(levels[4], block, channels[3], channels[4], 2, level_root=True, root_residual=residual_root)
        self.level5 = Tree(levels[5], block, channels[4], channels[5], 2, level_root=True, root_residual=residual_root)

        self.avgpool = nn.AvgPool2d(pool_size)
        self.fc = nn.Conv2d(channels[-1], num_classes, kernel_size=1, stride=1, padding=0, bias=True)

        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                n = m.kernel_size[0] * m.kernel_size[1] * m.out_channels
                m.weight.data.normal_(0, math.sqrt(2. / n))
            elif isinstance(m, nn.BatchNorm):
                m.weight.data.fill_(1)
                m.bias.data.zero_()

    def _make_conv_level(self, inplanes, planes, convs, stride=1, dilation=1):
        modules = []
        for i in range(convs):
            modules.extend([
                nn.Conv2d(inplanes,
                          planes,
                          kernel_size=3,
                          stride=stride if i == 0 else 1,
                          padding=dilation,
                          bias=False,
                          dilation=dilation),
                nn.BatchNorm2d(planes),
                nn.ReLU(inplace=True)
            ])
            inplanes = planes
        return nn.Sequential(*modules)
    
    def forward(self, x):
        y = []
        x = self.base_layer(x)
        for i in range(6):
            x = getattr(self, 'level{}'.format(i))(x)
            y.append(x)
        if self.return_levels:
            return y
        else:
            x = self.avgpool(x)
            x = self.fc(x)
            x = x.view(x.size(0), -1)
            return x

    def load_pretrained_model(self, data='imagenet', name='dla34', hashid='ba72cf86'):
        fc = self.fc
        if name.endswith('.pt'):
            model_weights = torch.load(data + name)
        else:
            raise ValueError("name is endswith '.pt'")
        num_classes = len(model_weights[list(model_weights.keys())[-1]])
        self.fc = nn.Conv2d(
            self.channels[-1], num_classes, kernel_size=1, stride=1, padding=0, bias=True
        )
        self.load_state_dict(model_weights)
        self.fc = fc



def dla34(pretrianed):
    model = DLA([1, 1, 1, 2, 2, 1], [16, 32, 64, 128, 256, 512], block=BasicBlock)
    if pretrianed:
        model.load_pretrained_model(data='imagenet', name='dla34', hashid='ba72cf86')
    return model

class Identity(nn.Module):
    def __init__(self) -> None:
        super(Identity, self).__init__()
    
    def forward(self, x):
        return x

def fill_up_weights(up):
    w = up.weight.data
    f = math.ceil(w.size(2) / 2)
    c = (2 * f - 1 - f % 2) / (2. * f)
    for i in range(w.size(2)):
        for j in range(w.size(3)):
            w[0, 0, i, j] = (1 - math.fabs(i / f - c)) * (1 - math.fabs(j / f - c))
    for c in range(1, w.size(0)):
        w[c, 0, :, :] = w[0, 0, :, :]


class IDAup(nn.Module):
    def __init__(self, node_kernel, out_dim, channels, up_factors) -> None:
        super().__init__()
        self.channels = channels
        self.out_dim = out_dim
        for i, c in enumerate(channels):
            if c == out_dim:
                proj = Identity()
            else:
                proj = nn.Sequential(
                    nn.Conv2d(c, out_dim, kernel_size=1, stride=1, bias=False),
                    nn.BatchNorm2d(out_dim),
                    nn.ReLU(inplace=True)
                )
            f = int(up_factors)
            if f == 1:
                up = Identity()
            else:
                up = nn.ConvTranspose2d(
                    out_dim, out_dim, f * 2, stride=f, padding=f // 2, output_padding=0, groups=out_dim, bias=False
                )
                fill_up_weights(up)
            setattr(self, 'proj_' + str(i), proj)
            setattr(self, 'up_' + str(i), up)
        
        for i in range(1, len(channels)):
            node = nn.Sequential(
                nn.Conv2d(out_dim * 2, out_dim, kernel_size=node_kernel, stride=1, padding=node_kernel // 2, bias=False),
                nn.BatchNorm2d(out_dim),
                nn.ReLU(inplace=True)
            )
            setattr(self, 'node_' + str(i), node)
        
        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                n = m.kernel_size(0) * m.kernel_size[1] * m.out_channels
                m.weight.data.normal_(0, math.sqrt(2. / n))
            elif isinstance(m, nn.BatchNorm2d):
                m.weight.data.fill_(1)
                m.bias.data.zero_()
            else:
                pass
    def forward(self, layers):
        assert len(self.channels) == len(layers), '{} vs {} layers'.format(len(self.channels), len(layers))
        layers = list(layers)
        for i, l in enumerate(layers):
            upsample = getattr(self, 'up_' + str(i))
            project = getattr(self, 'proj_' + str(i))
            layers[i] = upsample(project(l))
        x = layers[0]
        y = []
        for i in range(1, len(layers)):
            node = getattr(self, 'node_' + str(i))
            x = node(torch.cat([x, layers[i]], 1))
            y.append(x)
        return x, y

class DLAup(nn.Module):
    def __init__(self, channels, scales=(1, 2, 4, 8, 16), in_channels=None) -> None:
        super().__init__()
        if in_channels is None:
            in_channels = channels
        
        self.channels = channels
        channels = list(channels)
        scales = np.array(scales, dtype=int)
        for i in range(len(channels) - 1):
            j = -i - 2
            setattr(self, 'ida_{}'.format(i), IDAup(3, channels[i], in_channels[j:], scales[j:] // scales[j]))
            scales[j+1:] = scales[j]
            in_channels[j + 1:] = [channels[j] for _ in channels[j+1: ]]
    
    def forward(self, layers):
        layers = list(layers)
        assert len(layers) > 1
        for i in range(len(layers) - 1):
            ida = getattr(self, 'ida_{}'.format(i))
            x, y = ida(layers[-i - 2:])
            layers[-i - 1:] = y
        return x
        