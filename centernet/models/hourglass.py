# -*- coding: utf-8 -*-
'''
# Created on 2021/01/13 10:23:40
# @filename: hourglass.py
# @author: tcxia
'''

import torch
import torch.nn as nn

class conv2d(nn.Module):
    def __init__(self, k, in_dim, out_dim, stride=1, with_bn=True) -> None:
        super().__init__()

        pad = (k - 1) // 2
        self.conv = nn.Conv2d(in_dim, out_dim, (k, k), padding=(pad, pad), stride=(stride, stride), bias=not with_bn)
        self.bn = nn.BatchNorm2d(out_dim) if with_bn else nn.Sequential()
        self.relu = nn.ReLU(inplace=True)

    def forward(self, x):
        out = self.conv(x)
        out = self.bn(out)
        out = self.relu(out)
        return out



### 残差结构
class residual(nn.Module):
    def __init__(self, k, in_dim, out_dim, stride=1, with_bn=True) -> None:
        super().__init__()
        
        self.conv1 = nn.Conv2d(in_dim, out_dim, (3, 3), padding=(1, 1), stride=(stride, stride), bias=False)
        self.bn1 = nn.BatchNorm2d(out_dim)
        self.relu1 = nn.ReLU(inplace=True)

        self.conv2 = nn.Conv2d(out_dim, out_dim, (3, 3), padding=(1, 1), bias=False)
        self.bn2 = nn.BatchNorm2d(out_dim)

        self.skip = nn.Sequential(
            nn.Conv2d(in_dim, out_dim, (1, 1), stride=(stride, stride), bias=False),
            nn.BatchNorm2d(out_dim)
        ) if stride != 1 or in_dim != out_dim else nn.Sequential()

        self.relu = nn.ReLU(inplace=True)

    def forward(self, x):
        out = self.conv1(x)
        out = self.bn1(out)
        out = self.relu1(out)

        out = self.conv2(out)
        out = self.bn2(out)

        skip = self.skip(x)

        return self.relu(skip + out)


class kp_module(nn.Module):
    def __init__(self, n, dims, modules) -> None:
        super().__init__()

        self.n = n

        curr_mod = modules[0]
        next_mod = modules[1]

        curr_dim = dims[0]
        next_dim = dims[1]

        # 将输入的特征层进行两次残差卷积，便于和后面的层进行融合
        self.up1 = make_layer(3, curr_dim, next_dim, curr_mod)

        # 进行下采样
        self.low1 = make_hg_layer(3, curr_dim, next_dim, curr_mod)

        # 构建U型结构的下一层
        if self.n > 1:
            self.low2 = kp_module(n-1, dims[1:], modules[1:])
        else:
            self.low2 = make_layer(3, next_dim, next_dim, next_mod)
        
        # 将U型结构下一层反馈上来的层进行残差卷积
        self.low3 = make_layer_revr(3, next_dim, curr_dim, curr_mod)

        # 将U型结构下一层反馈上来的进行上采样
        self.up2 = nn.Unsample(scale_factor=2)

    def forward(self, x):
        up1 = self.up1(x)
        low1 = self.low1(x)
        low2 = self.low2(low1)
        low3 = self.low3(low2)
        up2 = self.up2(low3)
        out = up1 + up2
        return out

def make_layer(k, in_dim, out_dim, modules):
    layers = [residual(k, in_dim, out_dim)]
    for _ in range(modules - 1):
        layers.append(residual(k, out_dim, out_dim))
    return nn.Sequential(*layers)

def make_hg_layer(k, in_dim, out_dim, modules):
    layers = [residual(k, in_dim, out_dim, stride=2)]
    for _ in range(modules - 1):
        layers += [residual(k, out_dim, out_dim)]
    return nn.Sequential(*layers)

def make_layer_revr(k, in_dim, out_dim, modules):
    layers = []
    for _ in range(modules - 1):
        layers.append(residual(k, in_dim, in_dim))
    layers.append(residual(k, in_dim, out_dim))
    return nn.Sequential(*layers)