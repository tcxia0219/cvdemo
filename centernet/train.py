# -*- coding: utf-8 -*-
'''
# Created on 2021/01/12 10:29:48
# @filename: train.py
# @author: tcxia
'''

import torch
from torch.autograd import Variable
import torch.utils.data as tud

from models.losses import focal_loss, reg_l1_loss
from data.datasets import CenternetDataset
from models.centernet import CenterNet_Resnet


device = torch.device("cuda:3" if torch.cuda.is_available() else "cpu")

def Trainer(model, optimizer, train_loader, epoches, device):

    model.train()
    for epoch in range(epoches):
        for i, batch in enumerate(train_loader):
            if torch.cuda.is_available():
                batch = [Variable(torch.from_numpy(b).type(torch.FloatTensor)).to(device) for b in batch]
            else:
                batch = [Variable(torch.from_numpy(b).type(torch.FloatTensor)) for b in batch]

            batch_imgs, batch_hms, batch_whs, batch_regs, batch_reg_masks = batch

            optimizer.zero_grad()

            hm, wh, offset = model(batch_imgs)
            c_loss = focal_loss(hm, batch_hms)
            wh_loss = 0.1 * reg_l1_loss(wh, batch_whs, batch_reg_masks)
            off_loss = reg_l1_loss(offset, batch_regs, batch_reg_masks)

            loss = c_loss + wh_loss + off_loss

            loss.backward()

            optimizer.step()

            if (i + 1) % 10 == 0:
                print("Epoch: {} | Iteration: {} | Loss: {}".format(epoch, i + 1, loss.item()))



if __name__ == "__main__":
    train_data = '/data/line/voc/centernet.data'
    with open(train_data, 'r') as f_in:
        data = f_in.readlines()

    input_shape = (512, 512, 3)
    num_classes = 6
    train_set = CenternetDataset(data, input_shape, num_classes)
    train_loader = tud.DataLoader(train_set, batch_size=4, pin_memory=True, collate_fn=train_set.collate)

    pretrained = False
    model = CenterNet_Resnet(pretrained, num_classes=num_classes)
    model.to(device)

    optimizer = torch.optim.Adam(model.parameters(), lr=0.01, weight_decay=5e-4)

    Trainer(model, optimizer, train_loader, 10, device)
