# -*- coding: utf-8 -*-
'''
# Created on 12月-15-20 12:43
# @filename: voc.py
# @author: tcxia
'''

import os
import numpy as np
from PIL import Image
import xml.etree.ElementTree as ET

import torch
import torch.utils.data as tud
from skimage import transform as sktsf
from torchvision import transforms as tvtsf
from torchvision.transforms.transforms import ToTensor

CLASSES = ('aeroplane', 'bicycle', 'bird', 'boat', 'bottle', 'bus', 'car',
           'cat', 'chair', 'cow', 'diningtable', 'dog', 'horse', 'motorbike',
           'person', 'pottedplant', 'sheep', 'sofa', 'train', 'tvmonitor')

class VOCDataset(tud.Dataset):
    def __init__(self, data_dir, split='trainval') -> None:
        super().__init__()
        trainval_file = os.path.join(data_dir, 'ImageSets/Main/{0}.txt'.format(split))
        files = open(trainval_file).readlines()
        self.ids = [i.strip() for i in files]
        self.data_dir = data_dir
        self.classes = CLASSES

    def __getitem__(self, index):
        file = self.ids[index]
        anno = ET.parse(os.path.join(self.data_dir, 'Annotations', file + '.xml'))
        bbox = list()
        label = list()
        for obj in anno.findall('object'):
            bndbox = obj.find('bndbox')
            bbox.append([int(bndbox.find(tag).text) - 1 for tag in ['ymin', 'xmin', 'ymax', 'xmax']])
            name = obj.find('name').text.lower().strip()
            label.append(self.classes.index(name))

        bbox = np.stack(bbox).astype(np.float32)
        label = np.stack(label).astype(np.int32)

        img_file = os.path.join(self.data_dir, 'JPEGImages', file + '.jpg')
        img = self.read_img(img_file)

        t_img, t_bbox, t_label, t_scale = self.transformer(img, bbox, label)

        return t_img, t_bbox, t_label, t_scale


    def __len__(self) -> int:
        return len(self.ids)


    def read_img(self, imgfile, color=True):
        f = Image.open(imgfile)
        try:
            if color:
                img = f.convert('RGB')
            else:
                img = f.convert('P')
            img = np.asarray(img, dtype=np.float32)
        finally:
            if hasattr(f, 'close'):
                f.close()
        if img.ndim == 2:
            return img[np.newaxis]
        else:
            return img.transpose((2, 0, 1))

    def transformer(self, img, bbox, label, min_size=600, max_size=1000):
        _, H, W = img.shape
        # print(H, W)
        img = self.preprocess(img)

        _, o_H, o_W = img.shape
        scale = o_H / H
        bbox = self.resize_bbox(bbox, (H, W), (o_H, o_W))
        return img, bbox, label, scale

    def preprocess(self, img, min_size=600, max_size=1000):
        C, H, W = img.shape
        scale1 = min_size / min(H, W)
        scale2 = max_size / max(H, W)
        scale = min(scale1, scale2)
        img = img / 255.

        img = sktsf.resize(img, (C, H*scale, W*scale), mode='reflect')

        normalize = tvtsf.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])

        return normalize(torch.from_numpy(img)).numpy()

    def resize_bbox(self, bbox, in_size, out_size):
        bbox = bbox.copy()
        y_scale = float(out_size[0]) / in_size[0]
        x_scale = float(out_size[1]) / in_size[1]
        bbox[:, 0] = y_scale * bbox[:, 0]
        bbox[:, 2] = y_scale * bbox[:, 2]
        bbox[:, 1] = x_scale * bbox[:, 1]
        bbox[:, 3] = x_scale * bbox[:, 3]
        return bbox


if __name__ == "__main__":
    voc_dir = '/data/voc/VOCdevkit/VOC2012'
    train_set = VOCDataset(voc_dir)
    train_dataloader = tud.DataLoader(train_set,
                                      batch_size=2,
                                      shuffle=True,
                                      num_workers=0,
                                      pin_memory=True)
    print(next(iter(train_dataloader)))
    # print(train_dataloader)