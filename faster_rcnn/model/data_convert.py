# -*- coding: utf-8 -*-
'''
# Created on 12月-29-20 17:22
# @filename: data_convert.py
# @author: tcxia
'''
import numpy as np
import torch


#将data转换为numpy格式
def tonumpy(data):
    if isinstance(data, np.ndarray):
        return data
    if isinstance(data, torch.Tensor):
        return data.detach().cpu().numpy()


#将data转换为tensor格式
def totensor(data, device='cpu'):
    if isinstance(data, np.ndarray):
        data = torch.from_numpy(data)
    if isinstance(data, torch.Tensor):
        data = data.detach()
    return data.to(device)


def scalar(data):
    if isinstance(data, np.ndarray):
        return data.reshape(1)[0]
    if isinstance(data, torch.Tensor):
        return data.item()