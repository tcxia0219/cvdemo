# -*- coding: utf-8 -*-
'''
# Created on 12月-29-20 15:16
# @filename: faster_rcnn_fpn.py
# @author: tcxia
'''

import torch
import torch.nn as nn

class FasterRCNN(nn.Module):
    def __init__(
        self,
        C2,
        C3,
        C4,
        fpn,
        rpn,
        head,
        loc_normalize_mean=(0., 0., 0., 0.),
        loc_normalize_std=(0.1, 0.1, 0.2, 0.2)
    ) -> None:
        super().__init__()
        self.C2 = C2
        self.C3 = C3
        self.C4 = C4
        self.fpn = fpn
        self.rpn = rpn
        self.head = head

        self.loc_normalize_mean = loc_normalize_mean
        self.loc_normalize_std = loc_normalize_std

    def forward(self, x, scale=1.):
        img_size = x.shape[2:]

        c2_out = self.C2(x)
        c3_out = self.C3(c2_out)
        c4_out = self.C4(c3_out)

        p2, p3, p4, p5 = self.fpn(c2_out, c3_out, c4_out)
        feature_maps = [p2, p3, p4, p5]
        rcnn_maps = [p2, p3, p4]
        rpn_locs, rpn_scores, rois, roi_indices, anchor = self.rpn(feature_maps, img_size, scale)
        roi_cls_locs, roi_scores = self.head(rcnn_maps, rois, roi_indices)
        return roi_cls_locs, roi_scores, roi_indices


    def get_optimizer(self, lr=0.01, weight_decay=0.98, use_adam=True):
        params = []
        for key, value in dict(self.named_parameters()).items():
            if value.requires_grad:
                if 'bias' in key:
                    params += [{'params':[value], 'lr': lr * 2, 'weight_decay':0}]
                else:
                    params += [{'params':[value], 'lr': lr, 'weight_decay': weight_decay}]   
        if use_adam:
            self.optimizer = torch.optim.Adam(params)
        else:
            self.optimizer = torch.optim.SGD(params, momentum=0.9)
        return self.optimizer