# -*- coding: utf-8 -*-
'''
# Created on 12月-29-20 15:51
# @filename: faster_rcnn_fpn_vgg16.py
# @author: tcxia
'''

import os
os.environ['TORCH_HOME'] = '/data/cv_models'


from model.faster_rcnn import FasterRCNN
from model.fpn import FPN
from model.vgg_head import decomv_vgg16_fpn
from model.rpn import RegionProposalNetwork
from model.vgg_head import VGG16ROIHead


class FasterRCNNVGG16FPN(FasterRCNN):
    def __init__(self, n_fg_class, ratios=[0.5, 1, 2], anchor_scales=[8, 16], feat_stride=[4, 8, 16, 32]) -> None:
        C2, C3, C4, classifer = decomv_vgg16_fpn()

        fpn = FPN(out_channels=512)

        rpn = RegionProposalNetwork(512, 512, ratios=ratios, anchor_scales=anchor_scales, feat_stride=feat_stride)

        head = VGG16ROIHead(n_class=n_fg_class + 1, roi_size=7, feat_stride=feat_stride, classifer=classifer)

        super(FasterRCNNVGG16FPN, self).__init__(C2, C3, C4, fpn, rpn, head)
