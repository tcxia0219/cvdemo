# -*- coding: utf-8 -*-
'''
# Created on 12月-15-20 13:02
# @filename: pre_vgg16.py
# @author: tcxia
'''

import os
os.environ['TORCH_HOME'] = '/data/cv_models'

import torch
import torch.nn as nn
from torchvision.models import vgg16

import torch.nn.functional as F

from model.faster_rcnn import FasterRCNN
from model.rpn import RegionProposalNetwork
from model.vgg_head import VGG16ROIHead, decomv_vgg16


class FasterRCNNVGG16(FasterRCNN):
    def __init__(self, n_fg_class=20, feat_stride=16, ratios=[0.5, 1, 1], anchor_scales=[8, 16, 32], device='cpu') -> None:
        extractor, classifer = decomv_vgg16()

        rpn = RegionProposalNetwork(512,
                                    512,
                                    ratios=ratios,
                                    anchor_scales=anchor_scales,
                                    feat_stride=feat_stride,
                                    proposal_params={'device': device})

        head = VGG16ROIHead(n_fg_class + 1,
                            roi_size=7,
                            spatial_scale=(1. / feat_stride),
                            classifier=classifer,
                            device=device)

        super(FasterRCNNVGG16, self).__init__(extractor, rpn, head)



