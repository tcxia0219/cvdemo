# -*- coding: utf-8 -*-
'''
# Created on 12月-29-20 15:22
# @filename: fpn.py
# @author: tcxia
'''


import torch.nn as nn
import torch.nn.functional as F

class FPN(nn.Module):
    def __init__(self, out_channels) -> None:
        super().__init__()
        self.out_channels = out_channels

        self.P5 = nn.MaxPool2d(kernel_size=1, stride=2, padding=0)

        self.P4_conv1 = nn.Conv2d(512, self.out_channels, kernel_size=1, stride=1, padding=0)
        self.P4_conv2 = nn.Conv2d(self.out_channels, self.out_channels, 3, 1, 1)

        self.P3_conv1 = nn.Conv2d(512, self.out_channels, kernel_size=1, stride=1, padding=0)
        self.P3_conv2 = nn.Conv2d(self.out_channels, self.out_channels, 3, 1, 1)

        self.P2_conv1 = nn.Conv2d(256, self.out_channels, kernel_size=1, stride=1, padding=0)
        self.P2_conv2 = nn.Conv2d(self.out_channels, self.out_channels, 3, 1, 1)

    def forward(self, C2, C3, C4):
        p4_out = self.P2_conv1(C4)
        
        p5_out = self.P5(p4_out)

        p3_out = self._upsample_add(p4_out, self.P3_conv1(C3))
        p2_out = self._upsample_add(p3_out, self.P2_conv1(C2))

        p4_out = self.P4_conv2(p4_out)
        p3_out = self.P3_conv2(p3_out)
        p2_out = self.P2_conv2(p2_out)

        return p2_out, p3_out, p4_out, p5_out

    def _upsample_add(self, x, y):
        _, _, H, W = y.size()
        return F.interpolate(x, size=(H, W), mode='bilinear') + y

    def _norm_init(self, m, mean, stddev, truncated=False):
        # truncated normal
        if truncated:
            m.weight.data.normal_().fmod_(2).mul_(stddev).add_(mean)
        else:
            # random normal
            m.weight.data.normal_(mean, stddev)
            m.bias.data.zero_()
