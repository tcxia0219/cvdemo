# -*- coding: utf-8 -*-
'''
# Created on 12月-30-20 11:08
# @filename: roi_align.py
# @author: tcxia
'''

from torch.autograd import Function

class RoIAlignFunc(Function):
    def __init__(self, aligned_height, aligned_width, spatial_scale) -> None:
        super().__init__()
        self.aligned_height = int(aligned_height)
        self.aligned_width = int(aligned_width)
        self.spatial_scale = float(spatial_scale)
        self.rois = None
        self.feature_size = None

    def forward(self, features, rois):
        self.rois = rois
        self.feature_size = features.size()

        batch_size, num_channels, data_height, data_width = features.size()
        num_rois = rois.size()

        output = features.new(num_rois, num_channels, self.aligned_height, self.aligned_width).zero_()

        return output

        

