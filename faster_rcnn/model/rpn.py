# -*- coding: utf-8 -*-
'''
# Created on 12月-15-20 13:08
# @filename: base.py
# @author: tcxia
'''

import numpy as np

import torch
import torch.nn as nn
import torch.nn.functional as F

from model.creator import ProposalCreator


def normal_init(m, mean, std, truncated=False):
    if truncated:
        m.weight.data.normal_().fmod_(2).mul_(std).add_(mean)
    else:
        m.weight.data.normal_(mean, std)
        m.bias.data.zero_()


class RegionProposalNetwork(nn.Module):
    def __init__(self,
                 in_channels=512,
                 mid_channels=512,
                 ratios=[0.5, 1, 2],
                 anchor_scales=[8, 16, 32],
                 feat_stride=16,
                 proposal_params=dict()) -> None:
        super().__init__()
        self.anchor_base = self.generator_anchor(base_size=16, ratios=ratios, anchor_scales=anchor_scales)
        self.feat_stride = feat_stride

        self.proposal_layer = ProposalCreator(self, **proposal_params)

        n_anchor = self.anchor_base.shape[0]

        self.conv1 = nn.Conv2d(in_channels, mid_channels, 3, 1, 1)
        self.score = nn.Conv2d(mid_channels, n_anchor * 2, 1, 1, 0)
        self.loc = nn.Conv2d(mid_channels, n_anchor * 4, 1, 1, 0)

        normal_init(self.conv1, 0, 0.01)
        normal_init(self.score, 0, 0.01)
        normal_init(self.loc, 0, 0.01)

    def forward(self, x, img_size, scale=1.):
        # x: [batch_size, in_channels, h, w]
        n, _, hh, ww = x.shape
        anchor = self.shift_anchor(np.array(self.anchor_base), self.feat_stride, hh, ww)

        n_anchor = anchor.shape[0] // (hh * ww)

        # [batch_size, mid_channels, h, w]
        h = F.relu(self.conv1(x))

        # [batch_size, n_anchor*4, h, w]
        rpn_locs = self.loc(h)
        # [batch_size, (h*w*n_anchor), 4]
        rpn_locs = rpn_locs.permute(0, 2, 3, 1).contiguous().view(n, -1, 4)

        # [batch_size, n_anchor*2, h, w]
        rpn_scores = self.score(h)
        rpn_scores = rpn_scores.permute(0, 2, 3, 1).contiguous()

        # [batch_size, h, w, n_anchor, 2]
        rpn_softmax_scores = F.softmax(rpn_scores.view(n, hh, ww, n_anchor, 2), dim=4)

        # [batch_size, h, w, n_anchor]
        rpn_fg_scores = rpn_softmax_scores[:, :, :, :, 1].contiguous()

        # [batch_size, h * w * n_anchor]
        rpn_fg_scores = rpn_fg_scores.view(n, -1)

        # [batch_size, (h*w*n_anchor), 2]
        rpn_scores = rpn_scores.view(n, -1, 2)

        rois = list()
        roi_indices = list()
        for i in range(n):
            roi = self.proposal_layer(rpn_locs[i].cpu().data.numpy(),
                                      rpn_fg_scores[i].cpu().data.numpy(),
                                      anchor,
                                      img_size,
                                      scale=scale)
            batch_index = i * np.ones((len(roi),), dtype=np.int32)
            rois.append(roi)
            roi_indices.append(batch_index)

        rois = np.concatenate(rois, axis=0)
        roi_indices = np.concatenate(roi_indices, axis=0)
        return rpn_locs, rpn_scores, rois, roi_indices, anchor


    def generator_anchor(self, base_size, ratios=[0.5, 1, 2], anchor_scales=[8, 16, 32]):
        px = base_size / 2.
        py = base_size / 2.

        anchor_base = np.zeros((len(ratios) * len(anchor_scales), 4), dtype=np.float32)

        for i in range(len(ratios)):
            for j in range(len(anchor_scales)):
                h = base_size * anchor_scales[j] * np.sqrt(ratios[i])
                w = base_size * anchor_scales[j] * np.sqrt(1. / ratios[i])

                index = i * len(anchor_scales) + j
                anchor_base[index, 0] = py - h / 2.
                anchor_base[index, 1] = px - w / 2.
                anchor_base[index, 2] = py + h / 2.
                anchor_base[index, 3] = px + w / 2.
        return anchor_base

    def shift_anchor(self, anchor_base, feat_stride, height, width):
        shift_y = np.arange(0, height * feat_stride, feat_stride)
        shift_x = np.arange(0, width * feat_stride, feat_stride)
        shift_x, shift_y = np.meshgrid(shift_x, shift_y)
        shift = np.stack((shift_y.ravel(), shift_x.ravel(), shift_y.ravel(), shift_x.ravel()), axis=1)

        A = anchor_base.shape[0]
        K = shift.shape[0] # height * width
        anchor = anchor_base.reshape((1, A, 4)) + shift.reshape((1, K, 4)).transpose(1, 0, 2)
        anchor = anchor.reshape((K * A, 4)).astype(np.float32)
        return anchor
