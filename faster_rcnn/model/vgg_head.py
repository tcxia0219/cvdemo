# -*- coding: utf-8 -*-
'''
# Created on 12月-29-20 17:11
# @filename: vgg_head.py
# @author: tcxia
'''
import torch
import torch.nn as nn
import numpy as np
# from torchvision.ops import RoIPool
from torchvision.models import vgg16

from model.data_convert import totensor
from model.roi_align import RoIAlignFunc


def normal_init(m, mean, std, truncated=False):
    if truncated:
        m.weight.data.normal_().fmod_(2).mul_(std).add_(mean)
    else:
        m.weight.data.normal_(mean, std)
        m.bias.data.zero_()

class VGG16ROIHead(nn.Module):
    def __init__(self,
                 n_class,
                 roi_size,
                 spatial_scale,
                 feat_stride,
                 classifier,
                 device='cpu') -> None:
        super().__init__()
        self.classifier = classifier
        self.cls_loc = nn.Linear(4096, n_class * 4)
        self.score = nn.Linear(4096, n_class)

        normal_init(self.cls_loc, 0, 0.001)
        normal_init(self.score, 0, 0.01)

        self.n_class = n_class
        self.roi_size = roi_size
        self.feat_stride = feat_stride
        self.spatial_scale = [1. / i for i in feat_stride]
        # self.spatial_scale = spatial_scale
        # self.roi = RoIPool((self.roi_size, self.roi_size), self.spatial_scale)

        self.device = device

    def forward(self, x, feature_maps, rois, roi_indices):
        roi_indices = totensor(roi_indices).float()
        rois = totensor(rois).float()

        roi_level = self._PyramidROI_Feat(rois)

        indices_rois = torch.cat([roi_indices[:, None], rois], dim=1)

        # yx -> xy
        # [batch_size, ymin, xmin, ymax, xmax] - > [batch_size, xmin, ymin, xmax, ymax]
        xy_indices_rois = indices_rois[:, [0, 2, 1, 4, 3]]
        indices_rois = xy_indices_rois.contiguous() # 把tensor变成在内存中连续分布的形式

        # pool = self.roi(x, indices_rois)
        # pool = pool.view(pool.size(0), -1)
        
        roi_pool_feats = []
        roi_to_levels = []
        for i, l in enumerate(range(2, 5)):
            if (roi_level == l).sum() == 0:
                continue
            idx_l = (roi_level == l).nonzero()
            roi_to_levels.append(idx_l)

            keep_indices_rois = indices_rois[idx_l]
            keep_indices_rois = keep_indices_rois.view(-1, 5)

            roi_align = RoIAlignFunc(self.roi_size, self.roi_size, self.spatial_scale[i])
            pool = roi_align(feature_maps[i], keep_indices_rois)
            roi_pool_feats.append(pool)
        roi_pool_feats = torch.cat(roi_pool_feats, 0)
        roi_to_levels = torch.cat(roi_to_levels, 0)
        roi_to_levels = roi_to_levels.squeeze()
        idx_sorted, order = torch.sort(roi_to_levels)
        roi_pool_feats = roi_pool_feats[order]

        pool = roi_pool_feats.view(roi_pool_feats.size(0), -1) # batch_size, CHW拉直

        fc = self.classifier(pool)
        roi_cls_locs = self.cls_loc(fc)
        roi_score = self.score(fc)
        return roi_cls_locs, roi_score

    def _PyramidROI_Feat(self, rois):
        roi_h = rois[:, 2] - rois[:, 0] + 1
        roi_w = rois[:, 3] - rois[:, 1] + 1
        roi_level = torch.log(torch.sqrt(roi_h * roi_w)/224.0) / np.log(2)
        roi_level = torch.round(roi_level + 4)
        roi_level[roi_level < 2] = 2
        roi_level[roi_level > 4] = 4
        return roi_level


def decomv_vgg16():
    model = vgg16(pretrained=True)

    features = list(model.features)[:30]

    for layer in features[:10]:
        for p in layer.parameters():
            p.requires_grad = False
    features = nn.Sequential(*features)

    classifer = list(model.classifier)
    # 删除最后全连阶层
    del classifer[-1]
    classifer = nn.Sequential(*classifer)

    return features, classifer


def decomv_vgg16_fpn(use_dropout=False):
    model = model = vgg16(pretrained=True)

    C2 = list(model.features)[:16]
    C3 = list(model.features)[16:23]
    C4 = list(model.features)[23:30]

    classifer = model.classifier
    classifer = list(classifer)
    del classifer[6]  # 删除最后一层分类层

    if use_dropout:
        del classifer[5]
        del classifer[2]

    classifer = nn.Sequential(*classifer)

    return nn.Sequential(*C2), nn.Sequential(*C3), nn.Sequential(*C4), classifer
