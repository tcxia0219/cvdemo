# -*- coding: utf-8 -*-
'''
# Created on 12月-15-20 15:39
# @filename: train.py
# @author: tcxia
'''
import torch
import torch.utils.data as tud

import numpy as np

from data.voc import VOCDataset
from model.pre_vgg16 import FasterRCNNVGG16, FasterRCNNTrainer



device = torch.device("cuda:3" if torch.cuda.is_available() else "cpu")

epoches = 10


def inverse_normalize(img):
    return (img * 0.225 + 0.45).clip(min=0, max=1) * 225

def Trainer(epoch, model, train_dataloader, device):
    model.reset_meter()
    for i, batch in enumerate(train_dataloader):
        img, bbox, label = batch
        img = img.to(device)
        bbox = bbox.to(device)
        label = label.to(device)

        print("Epoch:{}".format(epoch))
        model.train_step(img, bbox, label)




if __name__ == "__main__":
    voc_dir = '/data/voc/VOCdevkit/VOC2012'
    train_set = VOCDataset(voc_dir)
    train_dataloader = tud.DataLoader(train_set,
                                      batch_size=8,
                                      shuffle=True,
                                      num_workers=0,
                                      pin_memory=True)



    faster_rcnn = FasterRCNNVGG16(device=device)
    model = FasterRCNNTrainer(faster_rcnn, device=device)
    model = model.to(device)


    for epoch in range(epoches):
        Trainer(epoch, model, train_dataloader, device)
