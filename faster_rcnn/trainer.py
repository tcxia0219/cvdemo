# -*- coding: utf-8 -*-
'''
# Created on 12月-29-20 17:48
# @filename: trainer.py
# @author: tcxia
'''
import torch
import torch.nn as nn
import torch.nn.functional as F
from torchnet.meter import ConfusionMeter, AverageValueMeter
from collections import namedtuple

from model.data_convert import tonumpy, totensor
from model.creator import AnchorTarget, ProposalTargetCreator

LossTuple = namedtuple('LossTuple', [
    'rpn_loc_loss', 'rpn_cls_loss', 'roi_loc_loss', 'roi_cls_loss',
    'total_loss'
])


class FasterRCNNTrainer(nn.Module):
    def __init__(self,
                 faster_rcnn,
                 rpn_sigma=3.,
                 roi_sigma=1.,
                 device='cpu') -> None:
        super().__init__()
        self.faster_rcnn = faster_rcnn
        self.rpn_sigma = rpn_sigma
        self.roi_sigma = roi_sigma

        self.anchor_target_creator = AnchorTarget() # 从上万个anchor中挑选256个来训练rpn，其中正样本不超过128
        self.proposal_target_creator = ProposalTargetCreator() # 从rpn给的2000个框中挑出128个来训练roi head， 其中正样本不超过32个

        self.loc_normalize_mean = faster_rcnn.loc_normalize_mean
        self.loc_normalize_std = faster_rcnn.loc_normalize_std

        self.optimizer = faster_rcnn.get_optimizer(lr=0.01, weight_decay=0.99, use_adam=True)

        self.rpn_cm = ConfusionMeter(2)
        self.roi_cm = ConfusionMeter(21)
        self.meters = {k: AverageValueMeter() for k in LossTuple._fields}

        self.device = device

    def forward(self, imgs, bboxes, labels, scale):
        n = bboxes.shape[0] # batch_size
        if n != 1:
            raise ValueError('Currently only batch size 1 is supported!')

        _, _, H, W = imgs.shape
        img_size = (H, W)

        features = self.faster_rcnn.extractor(imgs)

        # FPN ========>
        c2_out = self.faster_rcnn.C2(imgs)
        c3_out = self.faster_rcnn.C3(c2_out)
        c4_out = self.faster_rcnn.C4(c3_out)

        p2, p3, p4, p5 = self.faster_rcnn.fpn(c2_out, c3_out, c4_out)
        feature_maps = [p2, p3, p4, p5]
        rcnn_maps = [p2, p3, p5]
        rpn_locs, rpn_scores, rois, roi_indices, anchor = self.faster_rcnn.rpn(
            feature_maps, img_size, scale)


        # rpn_locs: [H * W * 9, 4]
        # rpn_scores: [H * W * 9, 2]
        # rois: [2000, 4]
        # roi_indices
        # anchor: [H * W * 9, 4]
        # 计算 (H/16) x (W/16) x 9 (大概20000)个anchor属于前景的概率， 取前12000个并经过NMS得到2000个近似目标框Gt的坐标
        # roi: [2000, 4]
        rpn_locs, rpn_scores, rois, roi_indices, anchor = self.faster_rcnn.rpn(
            feature_maps, img_size, scale)

        bbox = bboxes[0]
        label = labels[0]
        rpn_score = rpn_scores[0]
        rpn_loc = rpn_locs[0]
        roi = rois

        sample_roi, gt_roi_loc, gt_roi_label = self.proposal_target_creator(
            roi, tonumpy(bbox), tonumpy(label), self.loc_normalize_mean,
            self.loc_normalize_std)

        sample_roi_index = torch.zeros(len(sample_roi))
        roi_cls_loc, roi_score = self.faster_rcnn.head(features, sample_roi,
                                                       sample_roi_index)


        # RPN loss
        gt_rpn_loc, gt_rpn_label = self.anchor_target_creator(
            tonumpy(bbox), anchor, img_size)
        gt_rpn_label = totensor(gt_rpn_label).long()
        gt_rpn_loc = totensor(gt_rpn_loc)

        # rpn 的回归 L1smooth损失
        rpn_loc_loss = self._fast_rcnn_loc_loss(rpn_loc, gt_rpn_loc,
                                                gt_rpn_label.data,
                                                self.rpn_sigma)
        # rpn的分类交叉熵损失
        rpn_cls_loss = F.cross_entropy(rpn_score,
                                       gt_rpn_label.to(self.device),
                                       ignore_index=-1)
        _gt_rpn_label = gt_rpn_label[gt_rpn_label > -1]
        _rpn_score = tonumpy(rpn_score)[tonumpy(gt_rpn_label) > -1]
        self.rpn_cm.add(totensor(_rpn_score), _gt_rpn_label.data.long())

        # ROI loss
        n_sample = roi_cls_loc.shape[0]
        roi_cls_loc = roi_cls_loc.view(n_sample, -1, 4)
        roi_loc = roi_cls_loc[torch.arange(0, n_sample).long().to(self.device),
                              totensor(gt_roi_label).long()]
        gt_roi_label = totensor(gt_roi_label).long()
        gt_roi_loc = totensor(gt_roi_loc)

        # roi的回归L1Smooth损失
        roi_loc_loss = self._fast_rcnn_loc_loss(roi_loc.contiguous(),
                                                gt_roi_loc, gt_roi_label.data,
                                                self.roi_sigma)

        # roi的分类交叉熵损失
        roi_cls_loss = nn.CrossEntropyLoss()(roi_score,
                                             gt_roi_label.to(self.device))

        self.roi_cm.add(totensor(roi_score), gt_roi_label.data.long())

        losses = [rpn_loc_loss, rpn_cls_loss, roi_loc_loss, roi_cls_loss]
        losses = losses + [sum(losses)]

        return LossTuple(*losses)

    def _fast_rcnn_loc_loss(self, pred_loc, gt_loc, gt_label, sigma):
        in_weight = torch.zeros(gt_loc.shape).to(self.device)

        in_weight[(gt_label > 0).view(-1, 1).expand_as(in_weight).to(
            self.device)] = 1

        loc_loss = self._smooth_l1_loss(pred_loc, gt_loc, in_weight.detach(),
                                        sigma)

        loc_loss /= ((gt_label >= 0).sum().float())
        return loc_loss

    def _smooth_l1_loss(self, x, t, in_weight, sigma):
        sigma = sigma**2
        diff = in_weight * (x - t)
        abs_diff = diff.abs()
        flag = (abs_diff.data < (1. / sigma)).float()
        y = (flag * (sigma / 2.) * (diff**2) + (1 - flag) *
             (abs_diff - 0.5 / sigma))
        return y.sum()

    def reset_meter(self):
        for key, meter in self.meters.items():
            meter.reset()

        self.roi_cm.reset()
        self.rpn_cm.reset()

    def update_meter(self, losses):
        loss_d = {k: self.scalar(v) for k, v in losses._asdict().items()}
        for key, meter in self.meters.items():
            meter.add(loss_d[key])

    def train_step(self, imgs, bboxes, labels, scale=0.5):
        self.optimizer.zero_grad()

        losses = self.forward(imgs, bboxes, labels, scale)
        losses.total_loss.backward()
        self.optimizer.step()
        self.update_meter(losses)

        print(
            "rpn_loc_loss:{:.4f} | rpn_cls_loss:{:.4f} | roi_loc_loss:{:.4f} | roi_cls_loss:{:.4f}"
            .format(losses.rpn_loc_loss, losses.rpn_cls_loss,
                    losses.roi_loc_loss, losses.roi_cls_loss))
