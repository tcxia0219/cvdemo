# -*- coding: utf-8 -*-
'''
# Created on 12月-22-20 15:13
# @filename: fpn.py
# @author: tcxia
'''



import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable



class FPNModel(nn.Module):
    def __init__(self, block, num_blocks) -> None:
        super().__init__()
        # num_blocks: [2, 2, 2, 2]

        self.in_planes = 64

        self.conv1 = nn.Conv2d(3, 64, kernel_size=7, stride=2, padding=3, bias=False)
        self.bn1 = nn.BatchNorm2d(64)

        self.layer1 = self._make_layer(block, 64, num_blocks[0], stride=1)
        self.layer2 = self._make_layer(block, 128, num_blocks[1], stride=2)
        self.layer3 = self._make_layer(block, 256, num_blocks[2], stride=2)
        self.layer4 = self._make_layer(block, 512, num_blocks[3], stride=2)

        self.toplayer = nn.Conv2d(2048, 256, kernel_size=1, stride=1, padding=0)

        # Smooth layer 平滑层
        # 作用： 在融合之后还会再采用3x3的卷积核对每个融合结果进行卷积，目的是消除上采样的混叠效应
        self.smooth1 = nn.Conv2d(256, 256, kernel_size=3, stride=1, padding=1)
        self.smooth2 = nn.Conv2d(256, 256, kernel_size=3, stride=1, padding=1)
        self.smooth3 = nn.Conv2d(256, 256, kernel_size=3, stride=1, padding=1)

        # Lateral layer 侧边层
        # 作用： 1*1的卷积核的主要作用是减少卷积核的个数，也就是减少feature map的个数，并不改变feature map的尺寸大小
        self.latlayer1 = nn.Conv2d(1024, 256, kernel_size=1, stride=1, padding=0)
        self.latlayer2 = nn.Conv2d(512, 256, kernel_size=1, stride=1, padding=0)
        self.latlayer3 = nn.Conv2d(256, 256, kernel_size=1, stride=1, padding=0)

    def forward(self, inputs):
        # inputs: [1, 3, 600, 900]
        c1 = F.relu(self.bn1(self.conv1(inputs))) # [1, 64, 300, 450]
        c1 = F.max_pool2d(c1, kernel_size=3, stride=2, padding=1) # [1, 64, 150, 225]
        c2 = self.layer1(c1) # [1, 4 * 64, 150, 225]
        c3 = self.layer2(c2)
        c4 = self.layer3(c3)
        c5 = self.layer4(c4)

        p5 = self.toplayer(c5)
        p4 = self._upsample_add(p5, self.latlayer1(c4))
        p3 = self._upsample_add(p4, self.latlayer2(c3))
        p2 = self._upsample_add(p3, self.latlayer3(c2))

        p4 = self.smooth1(p4)
        p3 = self.smooth2(p3)
        p2 = self.smooth3(p2)

        return p2, p3, p4, p5
    
    def _upsample_add(self, x, y):
        """上采样并将两个feature maps求和

        Args:
            x ([Variable]): [将要上采样的 上层feature map]
            y ([Variable]): [侧边的feature map]

        Returns:
            [Variable]: [相加之后的feature map]
        """
        _, _, H, W = y.size()
        return F.upsample(x, size=(H, W), mode='bilinear') + y

        
    def _make_layer(self, block, planes, num_blocks, stride):
        """resnet网络

        Args:
            block ([type]): [description]
            planes ([type]): [description]
            num_blocks ([type]): [description]
            stride ([type]): [description]

        Returns:
            [type]: [description]
        """
        strides = [stride] + [1] * (num_blocks - 1) # [stride, 1, 1, 1]
        layers = []
        for stride in strides:
            layers.append(block(self.in_planes, planes, stride))
            self.in_planes = planes * block.expansion
        return nn.Sequential(*layers)

    


class Bottleneck(nn.Module):
    expansion = 4
    
    def __init__(self, in_planes, planes, stride=1) -> None:
        super().__init__()
        # layer1 : in_planes=64, planes=64, stride=1
        self.conv1 = nn.Conv2d(in_planes, planes, kernel_size=1, bias=False)
        self.bn1 = nn.BatchNorm2d(planes)
        self.conv2 = nn.Conv2d(planes, planes, kernel_size=3, stride=stride, padding=1, bias=False)
        self.bn2 = nn.BatchNorm2d(planes)
        self.conv3 = nn.Conv2d(planes, self.expansion*planes, kernel_size=1, bias=False)
        self.bn3 = nn.BatchNorm2d(self.expansion*planes)

        self.shortcut = nn.Sequential()
        if stride != 1 or in_planes != self.expansion*planes:
            self.shortcut = nn.Sequential(
                nn.Conv2d(in_planes, self.expansion*planes, kernel_size=1, stride=stride, bias=False),
                nn.BatchNorm2d(self.expansion*planes)
            )

    def forward(self, x):
        # layer1: x: [1, 64, 150, 225]
        out = F.relu(self.bn1(self.conv1(x))) # [1, 64, 150, 225]
        out = F.relu(self.bn2(self.conv2(out))) # [1, 64, 150, 225]

        out = self.bn3(self.conv3(out)) # [1, 4*64, 150, 225]
        out += self.shortcut(x) # [1, 4 * 64, 150, 225] + [1, 4 * 64, 150, 225]
        out = F.relu(out)
        return out


def FPN101():
    return FPNModel(Bottleneck, [2, 2, 2, 2])

if __name__ == "__main__":
    net = FPN101()
    fms = net(Variable(torch.randn(1, 3, 600, 900)))
    for fm in fms:
        print(fm.size())
