# -*- coding: utf-8 -*-
'''
# Created on 12月-31-20 15:53
# @filename: coco_dataset.py
# @author: tcxia
'''
import os
import random
import numpy as np
from PIL import Image
import torch
import torch.nn.functional as F
import torch.utils.data as tud
from torchvision import transforms


def pad_to_square(img, pad_value):
    c, h, w = img.shape
    dim_diff = np.abs(h - w)
    pad1, pad2 = dim_diff//2, dim_diff - dim_diff//2
    pad = (0, 0, pad1, pad2) if h <= w else (pad1, pad2, 0, 0)
    img = F.pad(img, pad, 'constant', value=pad_value)
    return img, pad

def horisontal_filp(imgs, targets):
    imgs = torch.flip(imgs, [-1])
    targets[:, 2] = 1 - targets[:, 2]
    return imgs, targets

def Resize(img, size):
    img = F.interpolate(img.unsqueeze(0), size=size, mode='nearest').squeeze(0)
    return img

class COCODataset(tud.Dataset):
    def __init__(self, file_path, img_size=416, aug=True, multi_scale=True, normlized_labels=True) -> None:
        super().__init__()
        with open(file_path, 'r') as f:
            self.files = f.readlines()

        self.img_files = [file.replace('Annotations', 'JPEGImages').replace('.xml', '.jpg') for file in self.files]
        self.label_files = [file.replace('Annotations', 'labels').replace('.xml', '.txt') for file in self.files]

        self.img_size = img_size
        self.max_obj = 100
        self.aug = aug
        self.multi_scale = multi_scale
        self.normalized_labels = normlized_labels
        self.min_size = self.img_size - 3 * 32
        self.max_size = self.img_size + 3 * 32
        self.batch_count = 0


    def __len__(self) -> int:
        return len(self.img_files)

    def __getitem__(self, index):
        img_path = self.img_files[index % len(self.img_files)].rstrip()
        img = transforms.ToTensor()(Image.open(img_path).convert('RGB'))

        if len(img.shape) != 3:
            img = img.unsqueeze(0)
            img = img.expand(3, img.shape[1:])

        _, h, w = img.shape
        h_factor, w_factor = (h, w) if self.normalized_labels else (1, 1)
        img, pad = pad_to_square(img, 0)
        _, padded_h, padded_w = img.shape


        label_path = self.label_files[index % len(self.label_files)].rstrip()
        targets = None

        # assert os.path.exists(label_path)

        if os.path.exists(label_path):
            boxes = torch.from_numpy(np.loadtxt(label_path).reshape(-1, 5))

            x1 = w_factor * (boxes[:, 1] - boxes[:, 3] / 2)
            y1 = h_factor * (boxes[:, 2] - boxes[:, 4] / 2)

            x2 = w_factor * (boxes[:, 1] + boxes[:, 3] / 2)
            y2 = h_factor * (boxes[:, 2] + boxes[:, 4] / 2)

            x1 += pad[0]
            y1 += pad[2]
            x2 += pad[1]
            y2 += pad[3]

            boxes[:, 1] = ((x1 + x2) / 2) / padded_w
            boxes[:, 2] = ((y1 + y2) / 2) / padded_h
            boxes[:, 3] *= w_factor / padded_w
            boxes[:, 4] *= h_factor / padded_h

            targets = torch.zeros((len(boxes), 6))
            targets[:, 1:] = boxes


        if self.aug:
            if np.random.random() < 0.5:
                img, targets = horisontal_filp(img, targets)

        return img_path, img, targets


    def collate_fn(self, batch):
        paths, imgs, targets = list(zip(*batch))

        targets = [boxes for boxes in targets if boxes is not None]
        for i, boxes in enumerate(targets):
            boxes[:, 0] = i
        targets = torch.cat(targets, 0)

        if self.multi_scale and self.batch_count % 10 == 0:
            self.img_size = random.choice(range(self.min_size, self.max_size + 1, 32))

        imgs = torch.stack([Resize(img, self.img_size) for img in imgs])
        self.batch_count += 1
        return paths, imgs, targets

if __name__ == "__main__":
    file_path = '/data/line/voc/yolo.data'
    data = COCODataset(file_path)
    img_path, img, targets = next(iter(data))
    print(img_path)
    print(img.shape)
    print(targets)