# -*- coding: utf-8 -*-
'''
# Created on 1月-01-21 11:40
# @filename: covert_data.py
# @author: tcxia
'''

import os
import xml.etree.ElementTree as ET

classes = ["duangu", "yiwu", "sunshang", "songgu", "xiushi", "zhaxiansongsan"]



def convert(size, box):
    dw = 1. / (size[0])
    dh = 1. / (size[1])
    x = (box[0] + box[2]) / 2.0 - 1
    y = (box[1] + box[3]) / 2.0 - 1
    w = box[2] - box[0]
    h = box[3] - box[1]

    x = round(x * dw, 4)
    w = round(w * dw, 4)
    y = round(y * dh, 4)
    h = round(h * dh, 4)
    return (x, y, w, h)


def voc2yolo(xml_path, txt_path):
    tree = ET.parse(xml_path)
    root = tree.getroot()
    size = root.find('size')
    W = int(size.find('width').text)
    H = int(size.find('height').text)

    fw = open(txt_path, 'w')
    for obj in root.iter('object'):
        diffcult = obj.find('difficult').text
        if obj.find('name') is not None:
            clsname = obj.find('name').text
        else:
            clsname = obj.find('code').text
        if clsname not in classes or int(diffcult) == 1:
            continue
        cls_id = classes.index(clsname)
        xmlbox = obj.find('bndbox')
        box = (
            int(xmlbox.find('xmin').text),
            int(xmlbox.find('ymin').text),
            int(xmlbox.find('xmax').text),
            int(xmlbox.find('ymax').text))
        bbox = convert((W, H), box)

        # with open(txt_path, 'w') as fw:

        fw.write(str(cls_id) + " " + " ".join([str(i) for i in bbox]) + '\n')
    fw.close()

if __name__ == "__main__":
    xml_dir = '/data/line/voc/Annotations'
    txt_dir = '/data/line/voc/labels'
    file_path = '/data/line/voc/yolo.data'
    fw = open(file_path, 'w')
    xmls = os.listdir(xml_dir)
    for xml in xmls:
        xml_path = os.path.join(xml_dir, xml)
        xmlname = os.path.splitext(xml)[0]
        txt_path = os.path.join(txt_dir, xmlname + '.txt')
        voc2yolo(xml_path, txt_path)

        fw.write(xml_path + '\n')
    
    fw.close()
