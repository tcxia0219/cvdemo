# -*- coding: utf-8 -*-
'''
# Created on 1月-06-21 10:31
# @filename: dataset_spp.py
# @author: tcxia
'''

import os
from yolov3.tricks.mosaic import load_img

import numpy as np
from tqdm import tqdm
from PIL import Image
from pathlib import Path
import cv2

import torch
import torch.utils.data as tud


from utils.util import xywh2xyxy
from tricks.mosaic import load_mosaic

img_formats = ['.bmp', '.jpg', '.jpeg', '.png', '.tif', '.dng']

class COCOSPP(tud.Dataset):
    def __init__(self,
                 file_path,
                 img_size=416,
                 batch_size=16,
                 aug=False,
                 hyp=None,
                 rect=False, # Rectangular Training
                 cache=False,
                 single_cls=False,
                 pad=0.,
                 rank=-1) -> None:
        super().__init__()
        with open(file_path, 'r') as fr:
            info = fr.read().splitlines()

        self.img_files = [x for x in info if os.path.splittext(x)[-1].lower() in img_formats]
        self.n = len(self.img_files)
        assert self.n > 0, "No image detect"

        self.bi = np.floor(np.arange(n) / batch_size).astype(np.int)
        self.nb = bi[-1] + 1

        self.img_size = img_size
        self.aug = aug
        self.hyp = hyp
        self.rect = rect
        self.mosaic = self.aug and not self.rect

        self.label_files = [x.replace('images', 'labels').replace(os.path.splitext(x)[-1], '.txt') for x in self.img_files]

        sp = file_path.replace('.txt', '.shapes')
        try:
            with open(sp, 'r') as f:
                s = [x.split() for x in f.read().splitlines()]
                assert len(s) == self.n, 'shapefile out of aync'
        except Exception as e:
            if rank in [-1, 0]:
                img_files = tqdm(self.img_files, desc='Reading image shapes')
            else:
                img_files = self.img_files

            s = [Image.open(f).size for f in img_files]
            np.savetxt(sp, s, fmt='%g')

        self.shapes = np.array(s, dtype=np.float64)

        if self.rect:
            self.rect_func()

        self.imgs = [None] * self.n
        self.labels = [np.zeros((0, 5), dtype=np.float32)] * self.n

        extrac_bbox, labels_load = False, False
        nm, nf, ne, nd = 0, 0, 0, 0

        if rect is True:
            np_labels_path = str(Path(self.label_files[0]).parent) + '.rect.npy'
        else:
            np_labels_path = str(Path(self.label_files[0]).parent) + 'norect.npy'

        if os.path.isfile(np_labels_path):
            x = np.load(np_labels_path, allow_pickle=True)
            if len(x) == self.n:
                self.labels = x
                labels_load = True

        if rank in [-1, 0]:
            pbar = tqdm(self.label_files)
        else:
            pbar = self.label_files

        for i, file in enumerate(pbar):
            if labels_load:
                l = self.labels[i]
            else:
                try:
                    with open(file, 'r') as f:
                        l = np.array([x.strip() for x in f.read().splitlines()], dtype=np.float32)
                except Exception as e:
                    nm += 1
                    continue

            if l.shape[0]:
                assert l.shape[1] == 5, "label columns must be equal 5"
                assert (l >= 0).all(), "negative labels"
                assert (l[:, 1:] <= 1).all(), "non-normalized or out of bounds coord"

                if np.unique(l, axis=0).shape[0] < l.shape[0]:
                    nd += 1
                if single_cls:
                    l[:, 0] = 0

                self.labels[i] = l
                nf += 1

                if extrac_bbox:
                    p = Path(self.img_files[i])
                    img = cv2.imread(str(p))
                    h, w = img.shape[:2]
                    for j, x in enumerate(l):
                        f = "%s%sclassifier%s%g_%g_%s" % (p.parent.parent, os.sep, os.sep, x[0], j, p.name)
                        if not os.path.exists(Path(f).parent):
                            os.makedirs(Path(f).parent)

                        b = x[1:] * [w, h, w, h]
                        b[2:] = b[2:].max()
                        b[2:] = b[2:] * 1.3 + 30
                        b = xywh2xyxy(b.reshape(-1, 4)).revel().astype(np.int)

                        b[[0, 2]] = np.clip(b[[0, 2]], 0, w)
                        b[[1, 3]] = np.clip(b[[1, 3]], 0, h)
                        assert cv2.imwrite(f, img[[b[1]:b[3], b[0]:b[3]]]), "Failure extracting classifer boxes"
                else:
                    ne += 1


    def rect_func(self):
        s = self.shapes # [w, h]
        ar = s[:, 1] / s[:, 0]
        irect = ar.argsort()

        self.img_files = [self.img_files[i] for i in irect]
        self.label_files = [self.label_files[i] for i in irect]
        self.shapes = s[irect]
        ar = ar[irect]

        shapes = [[1, 1]] * self.nb
        for i in range(nb):
            ari = ar[self.bi == i]
            mini, maxi = ari.min(), ari.max()

            if maxi < 1:
                shapes[i] = [maxi, 1]
            elif mini > 1:
                shapes[i] = [1, 1 / mini]
        self.batch_shapes = np.ceil(np.array(shapes) * img_size / 32. + pad).astype(np.int) * 32

    def __len__(self) -> int:
        return len(self.img_files)
    
    def __getitem__(self, index: int) -> T_co:
        if self.mosaic:
            img, labels = load_mosaic(index, self.img_size, self.labels, self.img_files)
            shapes = None
        else:
            img, (h0, w0), (h, w) = self.load_image(index)

            shape = self.batch_shapes[self.bi[index]] if self.rect else self.img_size
            img, ratio, pad = 


    def load_image(self, index):
    # loads 1 image from dataset, returns img, original hw, resized hw
        img = self.imgs[index]
        if img is None:  # not cached
            path = self.img_files[index]
            img = cv2.imread(path)  # BGR
            assert img is not None, "Image Not Found " + path
            h0, w0 = img.shape[:2]  # orig hw
            # img_size 设置的是预处理后输出的图片尺寸
            r = self.img_size / max(h0, w0)  # resize image to img_size
            if r != 1:  # always resize down, only resize up if training with augmentation
                interp = cv2.INTER_AREA if r < 1 and not self.augment else cv2.INTER_LINEAR
                img = cv2.resize(img, (int(w0 * r), int(h0 * r)), interpolation=interp)
            return img, (h0, w0), img.shape[:2]  # img, hw_original, hw_resized
        else:
            return self.imgs[index], self.img_hw0[index], self.img_hw[index]  # img, hw_original, hw_resized
        
        