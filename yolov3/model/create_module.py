# -*- coding: utf-8 -*-
'''
# Created on 1月-05-21 16:59
# @filename: create_module.py
# @author: tcxia
'''
import torch.nn as nn

def create_modules(modules, device='cpu'):
    hyperparams = modules.pop(0)
    out_filters = [int(hyperparams['channels'])]

    routs = []
    module_list = nn.ModuleList()
    for i, m in enumerate(modules):
        module = nn.Sequential()
        if m['type'] == 'convolutional':
            bn = int(m['batch_normalize'])  # 1 or 0 / use or not
            filters = int(m['filters'])
            ks = int(m['size'])
            stride = int(m['stride']) if "stride" in m else (m['stride_y'],
                                                             m['stride_x'])
            # pad = int(m['pad'])
            module.add_module(
                f'conv_{i}',
                nn.Conv2d(
                    in_channels=out_filters[-1],
                    out_channels=filters,
                    kernel_size=ks,
                    stride=stride,
                    padding=(ks - 1) // 2 if m['pad'] else 0,
                    bias=not bn,
                ),
            )

            if bn:
                module.add_module(
                    f'batch_norm_{i}',
                    nn.BatchNorm2d(filters, momentum=0.9, eps=1e-5))
            else:
                routs.append(i)  # 如果该卷积层没有bn层，意味着该层为yolo的predictor
            if m['activation'] == 'leaky':
                module.add_module(f'leaky_{i}', nn.LeakyReLU(0.1,
                                                             inplace=True))

        elif m['type'] == 'maxpool':
            ks = int(m['size'])
            stride = int(m['stride'])
            # pad = int(m['pad'])
            pad = int((ks - 1) // 2)
            if ks == 2 and stride == 1:
                module.add_module(f'_debug_padding_{i}',
                                  nn.ZeroPad2d((0, 1, 0, 1)))
            maxpool = nn.MaxPool2d(kernel_size=ks, stride=stride, padding=pad)
            module.add_module(f'maxpool_{i}', maxpool)

        elif m['type'] == 'upsample':
            stride = int(m['stride'])
            upsample = Upsample(scale_factor=stride, mode='nearest')
            module.add_module(f'upsample_{i}', upsample)

        elif m['type'] == 'route':
            layers = [int(x) for x in m['layers'].split(',')]
            filters = sum([out_filters[1:][i] for i in layers])
            module.add_module(f'route_{i}', EmptyLayer())

        elif m['type'] == 'shortcut':
            filters = out_filters[1:][int(m['from'])]
            module.add_module(f'shortcut_{i}', EmptyLayer())

        elif m['type'] == 'yolo':
            anchor_idx = [int(x) for x in m['mask'].split(',')]

            anchors = [int(x) for x in m['anchors'].split(',')]
            anchors = [(anchors[i], anchors[i + 1])
                       for i in range(0, len(anchors), 2)]
            anchors = [anchors[i] for i in anchor_idx]
            # print(anchors)

            num_classes = int(m['classes'])
            # print(num_classes)
            img_size = int(hyperparams['height'])

            yolo_layer = YoloLayer(anchors, num_classes, img_size, self.device)
            module.add_module(f'yolo_{i}', yolo_layer)

        module_list.append(module)
        out_filters.append(filters)
    return hyperparams, module_list


class Upsample(nn.Module):
    def __init__(self, scale_factor, mode='nearest') -> None:
        super().__init__()
        self.scale_factor = scale_factor
        self.mode = mode

    def forward(self, x):
        x = F.interpolate(x, scale_factor=self.scale_factor, mode=self.mode)
        return x


class EmptyLayer(nn.Module):
    def __init__(self) -> None:
        super().__init__()


class FeatureConcat(nn.Module):
    def __init__(self, layers) -> None:
        super().__init__()
        self.layers = layers
        self.multiple = len(layers) > 1

    def forward(self, x, outputs):
        return torch.cat([outputs[i] for i in self.layers],
                         1) if self.multiple else outputs[self.layers[0]]


class WeightFeatureFusion(nn.Module):
    def __init__(self, layers, weight=False) -> None:
        super().__init__()
        self.layers = layers
        self.weight = weight
        self.n_layer = len(layers) + 1
        if weight:
            self.w = nn.Parameter(torch.zeros(self.n_layer),
                                  requires_grad=True)

    def forward(self, x, outputs):
        if self.weight:
            w = torch.sigmoid(self.w) * (2 / self.n_layer)
            x = x * w[0]

        nx = x.shape[1]
        for i in range(self.n_layer - 1):
            a = outputs[self.layers[i]] * w[i + 1] if self.weight else outputs[
                self.layers[i]]
            na = a.shape[1]

            if nx == na:
                x = x + a
            elif nx > na:
                x[:, :na] = x[:, :na] + a
            else:
                x = x + a[:, :nx]
        return x