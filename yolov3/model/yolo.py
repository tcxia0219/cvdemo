# -*- coding: utf-8 -*-
'''
# Created on 12月-31-20 10:28
# @filename: yolo.py
# @author: tcxia
'''

import numpy as np
import math
import torch
import torch.nn as nn
import torch.nn.functional as F

from utils.parser import parser_cfg
from utils.util import build_targets


def create_modules(modules, device='cpu'):
    hyperparams = modules.pop(0)
    out_filters = [int(hyperparams['channels'])]

    routs = []
    yolo_index = -1

    module_list = nn.ModuleList()
    for i, m in enumerate(modules):
        module = nn.Sequential()
        if m['type'] == 'convolutional':
            bn = m['batch_normalize'] # 1 or 0 / use or not
            filters = m['filters']
            ks = m['size']
            stride = m['stride'] if "stride" in m else (m['stride_y'], m['stride_x'])
            # pad = int(m['pad'])
            module.add_module(
                f'conv_{i}',
                nn.Conv2d(
                    in_channels=out_filters[-1],
                    out_channels=filters,
                    kernel_size=ks,
                    stride=stride,
                    padding=ks // 2 if m['pad'] else 0,
                    bias=not bn,
                ),
            )

            if bn:
                module.add_module(f'batch_norm_{i}', nn.BatchNorm2d(filters, momentum=0.9, eps=1e-5))
            else:
                routs.append(i) # 如果该卷积层没有bn层，意味着该层为yolo的predictor

            if m['activation'] == 'leaky':
                module.add_module(f'leaky_{i}', nn.LeakyReLU(0.1, inplace=True))

        elif m['type'] == 'maxpool':
            ks = m['size']
            stride = m['stride']
            # pad = int(m['pad'])
            pad = (ks - 1) // 2
            if ks == 2 and stride == 1:
                module.add_module(f'_debug_padding_{i}', nn.ZeroPad2d((0, 1, 0, 1)))
            maxpool = nn.MaxPool2d(kernel_size=ks, stride=stride, padding=pad)
            module.add_module(f'maxpool_{i}', maxpool)

        elif m['type'] == 'upsample':
            stride = m['stride']
            upsample = Upsample(scale_factor=stride, mode='nearest')
            module.add_module(f'upsample_{i}', upsample)

        elif m['type'] == 'route':
            # layers = [int(x) for x in m['layers'].split(',')]
            layers = m['layers']
            # filters = sum([out_filters[1:][i] for i in layers])
            filters = sum([out_filters[l + 1 if l > 0 else l] for l in layers])
            routs.extend([i + l if l < 0 else l for l in layers])
            # module.add_module(f'route_{i}', EmptyLayer())
            module.add_module(f'route_{i}', FeatureConcat(layers=layers))

        elif m['type'] == 'shortcut':
            # filters = out_filters[1:][int(m['from'])]
            layers = m['from']
            filters = out_filters[-1]
            routs.append(i + layers[0])
            module.add_module(f'shortcut_{i}', WeightFeatureFusion(layers=layers, weight='weight_type' in m))

        elif m['type'] == 'yolo':
            yolo_index += 1
            stride = [32, 16, 8]

            # anchor_idx = [int(x) for x in m['mask'].split(',')]
            # anchors = [int(x) for x in m['anchors'].split(',')]
            # anchors = [(anchors[i], anchors[i + 1]) for i in range(0, len(anchors), 2)]
            # anchors = [anchors[i] for i in anchor_idx]
            # # print(anchors)

            # num_classes = int(m['classes'])
            # # print(num_classes)
            # img_size = int(hyperparams['height'])

            # yolo_layer = YoloLayer(anchors, num_classes, img_size, self.device)

            yolo_layer = YOLOLayerSPP(
                anchors=m['anchors'][m['mask']],
                nc=m['classes'],
                stride=stride[yolo_index]
                )

            try:
                j = -1
                bias_ = module_list[j][0].bias
                bias = bias_.view(m.na, -1)
                bias[:, 4] += -4.5
                bias[:, 5:] += math.log(0.6 / (m.nc - 0.99))
                module_list[j][0].bias = nn.Parameter(bias_, requires_grad=bias_.requires_grad)
            except Exception as e:
                print('smart bias initalization failure.', e)
            module.add_module(f'yolo_{i}', yolo_layer)

        module_list.append(module)
        out_filters.append(filters)
    routs_bin = [False] * len(module_list)
    for i in routs:
        routs_bin[i] = True
    return hyperparams, module_list, routs_bin

class YoloLayer(nn.Module):
    def __init__(self, anchors, num_classes, img_dim=416, device='cpu') -> None:
        super().__init__()

        self.anchors = anchors

        self.num_anchors = len(anchors)
        self.num_classes = num_classes

        self.ignore_thres = 0.5
        self.mse_loss = nn.MSELoss()
        self.bce_loss = nn.BCELoss()

        self.obj_scale = 1
        self.noobj_scale = 100

        self.metrics = {}
        self.img_dim = img_dim
        self.grid_size = 0


        self.device = device

    def forward(self, x, target=None, img_dim=None):
        self.img_dim = img_dim
        num_samples = x.size(0)
        grid_size = x.size(2)
        # print(x.shape)

        FloatTensor = torch.cuda.FloatTensor if x.is_cuda else torch.FloatTensor

        prediction = (
            x.view(num_samples, self.num_anchors, self.num_classes + 5, grid_size, grid_size)
            .permute(0, 1, 3, 4, 2)
            .contiguous())

        x = torch.sigmoid(prediction[..., 0])
        y = torch.sigmoid(prediction[..., 1])
        w = prediction[..., 2]
        h = prediction[..., 3]
        pred_conf = torch.sigmoid(prediction[..., 4])
        pred_cls = torch.sigmoid(prediction[..., 5:])

        if grid_size != self.grid_size:
            self._compute_grid_offset(grid_size)

        # print(self.grid_x.shape)
        # print(x.data.shape)
        pred_boxes = FloatTensor(prediction[..., :4].shape).to(self.device)
        pred_boxes[..., 0] = x.data + self.grid_x
        pred_boxes[..., 1] = y.data + self.grid_y
        pred_boxes[..., 2] = torch.exp(w.data) * self.anchor_w
        pred_boxes[..., 3] = torch.exp(h.data) * self.anchor_h

        out = torch.cat(
            (
                pred_boxes.view(num_samples, -1, 4) * self.stride,
                pred_conf.view(num_samples, -1, 1),
                pred_cls.view(num_samples, -1, self.num_classes),
            ), -1,
        )

        if target is None:
            return out, 0
        else:
            iou_scores, cls_mask, obj_mask, noobj_mask, tx, ty, tw, th, tcls, tconf = build_targets(
                pred_boxes=pred_boxes,
                pred_cls=pred_cls,
                target=target,
                anchors=self.scaled_anchors,
                ignore_thres=self.ignore_thres,
                device=self.device
                )

            loss_x = self.mse_loss(x[obj_mask], tx[obj_mask])
            loss_y = self.mse_loss(y[obj_mask], ty[obj_mask])
            loss_w = self.mse_loss(w[obj_mask], tw[obj_mask])
            loss_h = self.mse_loss(h[obj_mask], th[obj_mask])

            loss_conf_obj = self.bce_loss(pred_conf[obj_mask], tconf[obj_mask])
            loss_conf_noobj = self.bce_loss(pred_conf[noobj_mask], tconf[noobj_mask])
            loss_conf = self.obj_scale * loss_conf_obj + self.noobj_scale * loss_conf_noobj


            # print(pred_conf[obj_mask].shape)
            # print(tcls[obj_mask].shape)
            loss_cls = self.bce_loss(pred_cls[obj_mask], tcls[obj_mask])

            total_loss = loss_x + loss_y + loss_w + loss_h + loss_conf + loss_cls

            cls_acc = 100 * cls_mask[obj_mask].mean()
            conf_obj = pred_conf[obj_mask].mean()
            conf_noobj = pred_conf[noobj_mask].mean()

            conf_50 = (pred_conf > 0.5).float()
            iou_50 = (iou_scores > 0.5).float()
            iou_75 = (iou_scores > 0.75).float()

            detected_mask = conf_50 * cls_mask * tconf

            precision = torch.sum(iou_50 * detected_mask) / (conf_50.sum() + 1e-16)
            recall_50 = torch.sum(iou_50 * detected_mask) / (obj_mask.sum() + 1e-16)
            recall_75 = torch.sum(iou_75 * detected_mask) / (obj_mask.sum() + 1e-16)

            self.merics = {
                'loss': total_loss.detach().cpu().item(),
                'x': loss_x.detach().cpu().item(),
                'y': loss_y.detach().cpu().item(),
                'w': loss_w.detach().cpu().item(),
                'h': loss_h.detach().cpu().item(),
                'conf': loss_conf.detach().cpu().item(),
                'cls': loss_cls.detach().cpu().item(),
                'cls_acc': cls_acc.detach().cpu().item(),
                'recall_50': recall_50.detach().cpu().item(),
                'recall_75': recall_75.detach().cpu().item(),
                'precision': precision.detach().cpu().item(),
                'conf_obj': conf_obj.detach().cpu().item(),
                'conf_noobj': conf_noobj.detach().cpu().item(),
                'grid_size': grid_size
            }

            return  out, total_loss


    def _compute_grid_offset(self, grid_size, cuda=True):
        self.grid_size = grid_size
        g = self.grid_size

        self.stride = self.img_dim / self.grid_size
        self.grid_x = torch.arange(g).repeat(g, 1).view([1, 1, g, g]).to(self.device)
        self.grid_y = torch.arange(g).repeat(g, 1).t().view([1, 1, g, g]).to(self.device)

        self.scaled_anchors = torch.Tensor([(a_w / self.stride, a_h / self.stride) for a_w, a_h in self.anchors]).to(self.device)
        self.anchor_w = self.scaled_anchors[:, 0:1].view((1, self.num_anchors, 1, 1))
        self.anchor_h = self.scaled_anchors[:, 1:2].view((1, self.num_anchors, 1, 1))


class YOLOLayerSPP(nn.Module):
    def __init__(self, anchors, nc, stride) -> None:
        super().__init__()
        self.anchors = torch.Tensor(anchors)
        self.stride = stride
        self.na = len(anchors)
        self.nc = nc
        self.no = nc + 5
        self.nx, self.ny, self.ng = 0, 0, (0, 0)
        self.anchor_vec = self.anchors / self.stride
        self.anchor_wh = self.anchor_vec.view(1, self.na, 1, 1, 2)
        self.grid = None
    
    def forward(self, x):
        bs, _, ny, nx = x.shape
        if (self.nx, self.ny) != (nx, ny) or self.grid is None:
            self._create_grid((nx, ny), x.device)

        x = x.view(bs, self.na, self.no, self.ny, self.nx).permute(0,1,3,4,2).contiguous()
        if self.training:
            return x

    def _create_grid(self, ng=(13, 13), device='cpu'):
        self.nx, self.ny = ng
        self.ng = torch.tensor(ng, dtype=torch.float)

        if not self.training:
            yv, xv = torch.meshgrid([
                torch.arange(self.ny, device=device),
                torch.arange(self.nx, device=device)
            ])
            self.grid = torch.stack((xv, yv), 2).view(
                (1, 1, self.ny, self.nx, 2)).float()

        if self.anchor_vec.device != device:
            self.anchor_vec = self.anchor_vec.to(device)
            self.anchor_wh = self.anchor_wh.to(device)


class Darknet(nn.Module):
    def __init__(self, cfg_path, img_size=416, device='cpu') -> None:
        super().__init__()
        self.device = device
        self.modules = parser_cfg(cfg_path) # 提取配置文件内容
        self.hyperparams, self.module_list, self.routs = create_modules(self.modules) # 提取超参数和构建模型
        # self.yolo_layers = [layer[0] for layer in self.module_list if hasattr(layer[0], 'metrics')]
        self.yolo_layers = self.get_yolo_layer()
        self.img_size = img_size
        self.seen = 0
        self.head = np.array([0, 0, 0, self.seen, 0], dtype=np.int32)


    def forward(self, x, targets=None):
        img_dim = x.shape[2]
        loss = 0
        layer_outputs, yolo_outputs = [], []
        for i, (m, module) in enumerate(zip(self.modules, self.module_list)):
            if m['type'] in ['convolutional', 'upsample', 'maxpool']:
                x = module(x)
            elif m['type'] == 'route':
                x = torch.cat([layer_outputs[int(layer_i)] for layer_i in m['layers'].split(',')], 1)
            elif m['type'] == 'shortcut':
                layer_i = int(m['from'])
                x = layer_outputs[-1] + layer_outputs[layer_i]
            elif m['type'] == 'yolo':
                x, layer_loss = module[0](x, targets, img_dim)
                loss += layer_loss
                yolo_outputs.append(x)
            layer_outputs.append(x)
        yolo_outputs = torch.cat(yolo_outputs, 1).detach().cpu()

        if targets is None:
            return yolo_outputs
        else:
            return (loss, yolo_outputs)

    def forward_once(self, x):
        yolo_out, out = [], []
        for i, module in enumerate(self.module_list):
            name = module.__class__.__name__
            if name in ['WeightFeatureFusion', 'FeatureConcat']:
                x = module(x, out)
            elif name == 'YOLOLayerSPP':
                yolo_out.append(module(x))
            else:
                x = module(x)

            out.append(x if self.routs[i] else [])
            
        if self.training:
            return yolo_out

    def get_yolo_layer(self):
        return [i for i, m in enumerate(self.module_list) if m.__class__.__name__=='YOLOLayerSPP']



class Upsample(nn.Module):
    def __init__(self, scale_factor, mode='nearest') -> None:
        super().__init__()
        self.scale_factor = scale_factor
        self.mode = mode

    def forward(self, x):
        x = F.interpolate(x, scale_factor=self.scale_factor, mode=self.mode)
        return x

class EmptyLayer(nn.Module):
    def __init__(self) -> None:
        super().__init__()


class FeatureConcat(nn.Module):
    def __init__(self, layers) -> None:
        super().__init__()
        self.layers = layers
        self.multiple = len(layers) > 1

    def forward(self, x, outputs):
        return torch.cat([outputs[i] for i in self.layers], 1) if self.multiple else outputs[self.layers[0]]

class WeightFeatureFusion(nn.Module):
    def __init__(self, layers, weight=False) -> None:
        super().__init__()
        self.layers = layers
        self.weight = weight
        self.n_layer = len(layers) + 1
        if weight:
            self.w = nn.Parameter(torch.zeros(self.n_layer), requires_grad=True)

    def forward(self, x, outputs):
        if self.weight:
            w = torch.sigmoid(self.w) * (2 / self.n_layer)
            x = x * w[0]

        nx = x.shape[1]
        for i in range(self.n_layer - 1):
            a = outputs[self.layers[i]] * w[i + 1] if self.weight else outputs[self.layers[i]]
            na = a.shape[1]

            if nx == na:
                x = x + a
            elif nx > na:
                x[:, :na] = x[:, :na] + a
            else:
                x = x + a[:, :nx]
        return x