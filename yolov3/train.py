# -*- coding: utf-8 -*-
'''
# Created on 12月-31-20 10:27
# @filename: train.py
# @author: tcxia
'''

import numpy as np
import torch
from torch.autograd import Variable
import torch.utils.data as tud

from model.yolo import Darknet
from data.coco_dataset import COCODataset
from utils.util import nms, get_batch_stat, ap_per_class, weight_init_norm_, xywh2xyxy, warmup_lr_scheduler

epoches = 10
batch_size = 2
learning_rate = 0.02
conf_thres = 0.5
nms_thres = 0.5
iou_thres = 0.5
img_size = 416
device = torch.device("cuda:2" if torch.cuda.is_available() else "cpu")

def Trainer(epoch, model, train_dataloader, optimizer, device):
    model.train()

    for i, batch in enumerate(train_dataloader):
        _, imgs, targets = batch

        imgs = Variable(imgs.to(device))
        targets = Variable(targets.to(device), requires_grad=False)

        optimizer.zero_grad()

        loss, outs = model(imgs, targets)
        loss.backward()

        optimizer.step()
        
        print("Epoch: {} | Iter: {} | Loss: {}".format(epoch, i, loss.item()))
        
       

def Evaler(model, dev_dataloader, device):
    model.eval()
    sample_matrics = []
    labels = []

    FloatTensor = torch.cuda.FloatTensor if torch.cuda.is_available() else torch.FloatTensor

    for i, batch in enumerate(dev_dataloader):
        _, imgs, targets = batch

        labels += targets[:, 1].tolist()
        targets[:, 2:] = xywh2xyxy(targets[:, 2:])
        targets[:, 2:] *= img_size

        imgs = Variable(imgs.type(FloatTensor).to(device), requires_grad=False)
        
        with torch.no_grad():
            outs = model(imgs)
            outs = nms(outs, conf_thres, nms_thres)
    
        sample_matrics += get_batch_stat(outs, targets, iou_thres)
    true_positives, pred_scores, pred_labels = [np.concatenate(x, 0) for x in list(zip(*sample_matrics))]
    precision, recall, ap, f1, ap_class =  ap_per_class(true_positives, pred_scores, pred_labels, labels)
    return precision, recall, ap, f1, ap_class


def Train_SPP(model, optimizer, data_loader, device, epoch, warmup=False):
    model.train()

    lr_scheduler = None
    if epoch == 0 and warmup:
        warmup_factor = 1.0 / 1000
        warmup_iters = min(1000, len(data_loader) - 1)

        lr_scheduler = warmup_lr_scheduler(optimizer, warmup_iters, warmup_factor)
        accumulate = 1
    
    mloss = torch.zeros(4).to(device)
    now_lr = 0.
    nb = len(data_loader)

    for i, batch in enumerate(data_loader):
        imgs, targets = batch
        imgs = imgs.to(device)
        targets = targets.to(device)

        


if __name__ == "__main__":
    train_path = '/data/line/voc/yolo.data'
    train_set = COCODataset(train_path, aug=True, multi_scale=True)
    train_dataloader = tud.DataLoader(train_set,
                                      batch_size=batch_size,
                                      shuffle=True,
                                      num_workers=0,
                                      pin_memory=True,
                                      collate_fn=train_set.collate_fn)

    cfg_path = 'configs/yolov3-custom.cfg'
    model = Darknet(cfg_path, device=device).to(device)
    model.apply(weight_init_norm_)

    optimizer = torch.optim.Adam(model.parameters(), lr=learning_rate)

    for epoch in range(epoches):
        Trainer(epoch, model, train_dataloader, optimizer, device)
        if (epoch + 1) % 2 == 0:
            print("Evaluating Model")
            precision, recall, ap, f1, ap_class = Evaler(model, train_dataloader, device)
            print("Prec: {} | Recall: {} | f1: {} | ap: {} | ap_class: {}".format(precision, recall, f1, ap, ap_class))

        torch.save(model.state_dict(), f'checkpoints/epoch_%d.pth' % epoch)