# -*- coding: utf-8 -*-
'''
# Created on 1月-05-21 11:07
# @filename: mosaic.py
# @author: tcxia
'''
import random
import cv2
import numpy as np


aug_flag  = True

# 将四张图片拼接到一张马赛克图像中
def load_mosaic(index, img_size, labels, img_files):
    labels_4= [] # 拼接图像的label信息
    xc, yc = [int(random.uniform(img_size * 0.5, img_size * 1.5)) for _ in range(2)]  # 随机初始化拼接图像的中心坐标
    indices = [index] + [random.randint(0, len(labels) - 1) for _ in range(3)] # 从dataset中随机找三张图片进行拼接
    for i, index in enumerate(indices):
        img, _, (h, w) = load_img(index, img_files, img_size)
        img_4 = np.full((img_size * 2, img_size * 2, img.shape[2]), 114, dtype=np.uint8)
        
        if i == 0:
            x1a, y1a, x2a, y2a = max(xc - w, 0), max(yc - h, 0), xc, yc
            x1b, y1b, x2b, y2b = w - (x2a - x1a), h - (y2a - y1a), w, h
        elif i == 1:
            x1a, y1a, x2a, y2a = xc, max(yc - h, 0), min(xc + w, img_size * 2), yc
            x1b, y1b, x2b, y2b = 0, h - (y2a - y1a), min(w, x2a - x1a), h
        elif i == 2:
            x1a, y1a, x2a, y2a = max(xc - w, 0), yc, xc, min(img_size * 2, yc + h)
            x1b, y1b, x2b, y2b = w - (x2a - x1a), 0, max(xc, w), min(y2a - y1a, h)
        else:
            x1a, y1a, x2a, y2a = xc, yc, min(xc + w, img_size * 2), min(img_size * 2, yc + h)
            x1b, y1b, x2b, y2b = 0, 0, min(w, x2a - x1a), min(y2a - y1a, h)
        
        img_4[y1a:y2a, x1a:x2a] = img[y1b:y2b, x1b:x2b]
        padw = x1a - x1b
        padh = y1a - y1b

        x = labels[index]
        labels = x.copy()
        if x.size > 0:
            labels[:, 1] = w * (x[:, 1] - x[:, 3] / 2) + padw
            labels[:, 2] = h * (x[:, 2] - x[:, 4] / 2) + padh
            labels[:, 3] = w * (x[:, 1] + x[:, 3] / 2) + padw
            labels[:, 4] = h * (x[:, 2] + x[:, 4] / 2) + padh
        labels_4.append(labels)

        if len(labels_4):
            labels_4 = np.concatenate(labels_4, 0)
            np.clip(labels_4[:, 1:], 0, 2 * img_size, out=labels_4[:, 1:])
        
        return img_4, labels_4


def load_img(index, img_files, img_size):
    path = img_files[index]
    img = cv2.imread(path)
    assert img is not None, "Image Not Found" + path
    h_ori, w_ori = img.shape[:2]
    r = img_size / max(h_ori, w_ori)
    interp = cv2.INTER_AREA if r < 1 and not aug_flag else cv2.INTER_LINEAR
    new_img = cv2.resize(img, (int(w_ori*r), int(h_ori*r)), interpolation=interp)
    return img, (h_ori, w_ori), new_img.shape[:2]
