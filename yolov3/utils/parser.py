# -*- coding: utf-8 -*-
'''
# Created on 12月-31-20 10:59
# @filename: parser_cfg.py
# @author: tcxia
'''
import os
import numpy as np

supported = [
    'type', 'batch_normalize', 'filters', 'size', 'stride', 'pad',
    'activation', 'layers', 'groups', 'from', 'mask', 'anchors', 'classes',
    'num', 'jitter', 'ignore_thresh', 'truth_thresh', 'random', 'stride_x',
    'stride_y', 'weights_type', 'weights_normalization', 'scale_x_y',
    'beta_nms', 'nms_kind', 'iou_loss', 'iou_normalizer', 'cls_normalizer',
    'iou_thresh', 'probability'
]

def parser_cfg(path):
    if not path.endswith('.cfg') or not os.path.exists(path):
        raise FileExistsError('the cfg file is not exist...')

    with open(path, 'r') as f:
        lines = f.read().split('\n')

    lines = [line for line in lines if line and not line.startswith('#')]
    lines = [line.strip() for line in lines]

    modules = []
    for line in lines:
        if line.startswith('['):
            modules.append({})
            modules[-1]['type'] = line[1:-1].strip()

            # 卷积模块，设置默认不使用BN
            if modules[-1]['type'] == 'convolutional':
                modules[-1]['batch_normalize'] = 0
        else:
            key, val = line.split('=')
            val = val.strip()
            key = key.strip()

            if key == 'anchors':
                val = val.replace(" ", "")
                modules[-1][key] = np.array([float(x) for x in val.split(",")]).reshape((-1, 2))
            elif (key in ['from', 'layers', 'mask']) or (key == 'size' and ',' in val):
                modules[-1][key] = [int(x) for x in val.split(',')]
            else:
                if val.isnumeric():
                    modules[-1][key] = int(val) if (int(val) - float(val)) == 0 else float(val)
                else:
                    modules[-1][key] = val
    return modules

if __name__ == "__main__":
    modules = parser_cfg('../configs/yolov3-custom.cfg')
    print(modules)
