# -*- coding: utf-8 -*-
'''
# Created on 12月-31-20 14:09
# @filename: util.py
# @author: tcxia
'''
import numpy as np
import torch
from tqdm import tqdm
import cv2


def weight_init_norm_(m):
    cls_name = m.__class__.__name__
    if cls_name.find('Conv') != -1:
        torch.nn.init.normal_(m.weight.data, 0.0, 0.02)
    elif cls_name.find('BatchNorm2d') != -1:
        torch.nn.init.normal_(m.weight.data, 1.0, 0.02)
        torch.nn.init.constant_(m.bias.data, 0.0)


def build_targets(pred_boxes,
                  pred_cls,
                  target,
                  anchors,
                  ignore_thres,
                  device='cpu'):

    BoolTensor = torch.cuda.BoolTensor if pred_boxes.is_cuda else torch.BoolTensor
    FloatTensor = torch.cuda.FloatTensor if pred_boxes.is_cuda else torch.FloatTensor

    nB = pred_boxes.size(0)
    nA = pred_boxes.size(1)
    nC = pred_cls.size(-1)
    nG = pred_boxes.size(2)

    obj_mask = BoolTensor(nB, nA, nG, nG).fill_(0).to(device)
    noobj_mask = BoolTensor(nB, nA, nG, nG).fill_(1).to(device)
    cls_mask = FloatTensor(nB, nA, nG, nG).fill_(0).to(device)
    iou_score = FloatTensor(nB, nA, nG, nG).fill_(0).to(device)
    tx = FloatTensor(nB, nA, nG, nG).fill_(0).to(device)
    ty = FloatTensor(nB, nA, nG, nG).fill_(0).to(device)
    tw = FloatTensor(nB, nA, nG, nG).fill_(0).to(device)
    th = FloatTensor(nB, nA, nG, nG).fill_(0).to(device)
    tcls = FloatTensor(nB, nA, nG, nG, nC).fill_(0).to(device)

    target_boxes = target[:, 2:6] * nG
    gxy = target_boxes[:, :2]
    gwh = target_boxes[:, 2:]

    ious = torch.stack([bbox_wh_iou(anchor, gwh) for anchor in anchors])
    best_ious, best_n = ious.max(0)

    b, target_labels = target[:, :2].long().t()
    gx, gy = gxy.t()
    gw, gh = gwh.t()
    gi, gj = gxy.long().t()

    obj_mask[b, best_n, gj, gi] = 1
    noobj_mask[b, best_n, gi, gi] = 0

    for i, anchor_ious in enumerate(ious.t()):
        noobj_mask[b[i], anchor_ious > ignore_thres, gj[i], gi[i]] = 0

    tx[b, best_n, gj, gi] = gx - gx.floor()
    ty[b, best_n, gj, gi] = gy - gy.floor()

    tw[b, best_n, gj, gi] = torch.log(gw / anchors[best_n][:, 0] + 1e-16)
    th[b, best_n, gj, gi] = torch.log(gh / anchors[best_n][:, 1] + 1e-16)

    tcls[b, best_n, gj, gi, target_labels] = 1

    cls_mask[b, best_n, gj,
             gi] = (pred_cls[b, best_n, gj,
                             gi].argmax(-1) == target_labels).float()

    iou_score[b, best_n, gj, gi] = bbox_iou(pred_boxes[b, best_n, gj, gi],
                                            target_boxes,
                                            x1y1x2y2=False)

    tconf = obj_mask.float()
    return iou_score, cls_mask, obj_mask, noobj_mask, tx, ty, tw, th, tcls, tconf


def bbox_wh_iou(wh1, wh2):
    wh2 = wh2.t()
    w1, h1 = wh1[0], wh1[1]
    w2, h2 = wh2[0], wh2[1]
    inter_area = torch.min(w1, w2) * torch.min(h1, h2)
    union_area = (w1 * h1 + 1e-16) + w2 * h2 - inter_area
    return inter_area / union_area


def bbox_iou(box1, box2, x1y1x2y2=True):
    if not x1y1x2y2:
        b1_x1, b1_x2 = box1[:, 0] - box1[:, 2] / 2, box1[:, 0] + box1[:, 2] / 2
        b1_y1, b1_y2 = box1[:, 1] - box1[:, 3] / 2, box1[:, 1] + box1[:, 3] / 2
        b2_x1, b2_x2 = box2[:, 0] - box2[:, 2] / 2, box2[:, 0] + box2[:, 2] / 2
        b2_y1, b2_y2 = box2[:, 1] - box2[:, 3] / 2, box2[:, 1] + box2[:, 3] / 2
    else:
        b1_x1, b1_y1, b1_x2, b1_y2 = box1[:, 0], box1[:, 1], box1[:,
                                                                  2], box1[:,
                                                                           3]
        b2_x1, b2_y1, b2_x2, b2_y2 = box2[:, 0], box2[:, 1], box2[:,
                                                                  2], box2[:,
                                                                           3]

    inter_rect_x1 = torch.max(b1_x1, b2_x1)
    inter_rect_y1 = torch.max(b1_y1, b2_y1)
    inter_rect_x2 = torch.min(b1_x2, b2_x2)
    inter_rect_y2 = torch.min(b1_y2, b2_y2)

    inter_area = torch.clamp(inter_rect_x2 - inter_rect_x1 + 1,
                             min=0) * torch.clamp(
                                 inter_rect_y2 - inter_rect_y1 + 1, min=0)

    b1_area = (b1_x2 - b1_x1 + 1) * (b1_y2 - b1_y1 + 1)
    b2_area = (b2_x2 - b2_x1 + 1) * (b2_y2 - b2_y1 + 1)

    iou = inter_area / (b1_area + b2_area - inter_area + 1e-16)
    return iou


def ap_per_class(tp, conf, pred_cls, target_cls):
    i = np.argsort(-conf)
    tp, conf, pred_cls = tp[i], conf[i], pred_cls[i]

    unique_cls = np.unique(target_cls)

    ap, p, r = [], [], []
    for c in tqdm(unique_cls, desc='Computing AP'):
        i = pred_cls == c
        n_gt = (target_cls == c).sum()
        n_p = i.sum()

        if n_p == 0 and n_gt == 0:
            continue

        elif n_p == 0 or n_gt == 0:
            ap.append(0)
            r.append(0)
            p.append(0)

        else:
            fpc = (1 - tp[i]).cumsum()
            tpc = (tp[i]).cumsum()

            recall_curve = tpc / (n_gt + 1e-16)
            r.append(recall_curve[-1])

            precision_curve = tpc / (tpc + fpc)
            p.append(precision_curve[-1])

            ap.append(compute_ap(recall_curve, precision_curve))

    p, r, ap = np.array(p), np.array(r), np.array(ap)
    f1 = 2 * p * r / (p + r + 1e-16)

    return p, r, ap, f1, unique_cls.astype('int32')


def compute_ap(recall, precision):
    mrec = np.concatenate(([0.], recall, [1.]))
    mpre = np.concatenate(([0.], precision, [0.]))

    for i in range(mpre.size - 1, 0, -1):
        mpre[i - 1] = np.maximum(mpre[i - 1], mpre[i])

    i = np.where(mrec[1:] != mrec[:-1])[0]

    ap = np.sum((mrec[i + 1] - mrec[i]) * mpre[i + 1])
    return ap


def xywh2xyxy(x):
    # y = x.new(x.shape)
    y = torch.zeros_like(x) if isinstance(x, torch.Tensor) else np.zeros_like(x)
    y[..., 0] = x[..., 0] - x[..., 2] / 2
    y[..., 1] = x[..., 1] - x[..., 3] / 2
    y[..., 2] = x[..., 0] + x[..., 2] / 2
    y[..., 3] = x[..., 1] + x[..., 3] / 2
    return y

def nms(preds, conf_thres=0.5, nms_thres=0.4):
    preds[..., :4] = xywh2xyxy(preds[..., :4])
    output = [None for _ in range(len(preds))]
    for i, pred in enumerate(preds):
        pred = pred[pred[:, 4] >= conf_thres]
        if not pred.size(0):
            continue
        
        score = pred[:, 4] * pred[:, 5:].max(1)[0]
        pred = pred[(-score).argsort()]
        cls_confs, cls_preds = pred[:, 5:].max(1, keepdim=True)
        detect = torch.cat((pred[:, :5], cls_confs.float(), cls_preds.float()), 1)

        keep_boxes = []
        while detect.size(0):
            lager_overlap = bbox_iou(detect[0, :4].unsqueeze(0), detect[:, :4]) > nms_thres
            label_match = detect[0, -1] == detect[:, -1]

            invaild = lager_overlap & label_match
            weights = detect[invaild, 4:5]

            detect[0, :4] = (weights * detect[invaild, :4]).sum(0) / weights.sum()
            keep_boxes += [detect[0]]

            detect = detect[~invaild]

        if keep_boxes:
            output[i] = torch.stack(keep_boxes)

    return output

def get_batch_stat(outputs, targets, iou_thres):
    batch_metrics = []
    for i in range(len(outputs)):
        if outputs[i] is None:
            continue
        
        output = outputs[i]
        pred_boxes = output[:, :4]
        pred_scores = output[:, 4]
        pred_labels = output[:, -1]

        true_positives = np.zeros(pred_boxes.shape[0])
        annos = targets[targets[:, 0] == i][:, 1:]
        target_labels = annos[:, 0] if len(annos) else []

        if len(annos):
            detected_boxes = []
            target_boxes = annos[:, 1:]
            for j, (pred_box, pred_label) in enumerate(zip(pred_boxes, pred_labels)):
                if len(detected_boxes) == len(annos):
                    break
                if pred_label not in target_labels:
                    continue
                
                iou, box_index = bbox_iou(pred_box.unsqueeze(0), target_boxes).max(0)
                if iou >= iou_thres and box_index not in detected_boxes:
                    true_positives[j] = 1
                    detected_boxes += [box_index]
        
        batch_metrics.append([true_positives, pred_scores, pred_labels])
    return batch_metrics


def warmup_lr_scheduler(optimizer, warmup_iter, warmup_factor):
    def f(x):
        if x >= warmup_iter:
            return 1
        alpha = float(x) / warmup_iter
        return warmup_factor * (1 - alpha) + alpha
    
    return torch.optim.lr_scheduler.LambdaLR(optimizer, lr_lambda=f)


def letterbox(img, new_shape=(416, 416), scale_up=True, auto=True, scale_fill=False, color=(114, 114, 114)):
    shape = img.shape[:2]
    if isinstance(new_shape, int):
        new_shape = (new_shape, new_shape)
    
    r = min(new_shape[0] / shape[0], new_shape[1] / shape[1])

    #对于大于指定输入大小的图片进行缩放，小于的不变
    if not scale_up:
        r = min(r, 1.0)

    ratio = r, r
    new_unpad = int(round(shape[1] * r)), int(round(shape[0] * r))
    dw, dh = new_shape[1] - new_unpad[0], new_shape[0] - new_unpad[1]

    if auto:
        dw, dh = np.mod(dw, 64), np.mod(dh, 64)
    elif scale_fill:
        dw, dh = 0, 0
        new_unpad = new_shape
        ratio = new_shape[0] / shape[1], new_shape[1] / shape[0]
    
    dw /= 2
    dh /= 2

    if shape[::-1] != new_unpad:
        img = cv2.resize(img, new_unpad, interpolation=cv2.INTER_LINEAR)

    top, bottom = int(round(dh - 0.1)), int(round(dh + 0.1))
    left, right = int(round(dw - 0.1)), int(round(dw + 0.1))

    img = cv2.copyMakeBorder(img, top, bottom, left, right, cv2.BORDER_CONSTANT, value=color)
    return img, ratio, (dw, dh)

    
