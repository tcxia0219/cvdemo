# -*- coding: utf-8 -*-
'''
# Created on 2021/01/06 16:07:46
# @filename: datasets.py
# @author: tcxia
'''

import numpy as np
from numpy.core.fromnumeric import shape
from tqdm import tqdm
from PIL import Image
import cv2

import torch
import torch.utils.data as tud

import sys
sys.path.append('..')

from tricks.rect import rect_func
from tricks.mosaic import mosaic, load_image
from utils.util import xyxy2xywh


class YOLODataset(tud.Dataset):
    def __init__(self, path, img_size=416, bs=16, aug=False, hyp=None, rect=False, img_weight=False, stride=32, pad=0.) -> None:
        super().__init__()

        with open(path, 'r') as fr:
            data = fr.read().splitlines()

        self.img_files = [d.replace('Annotations', 'JPEGImages').replace('.xml', '.jpg') for d in data]

        n = len(self.img_files)
        bi = np.floor(np.arange(n) / bs).astype(np.int)
        nb = bi[-1] + 1

        self.n = n
        self.batch = bi
        self.img_size = img_size
        self.aug = aug
        self.hyp = hyp
        # self.img_weights = img_weight

        # self.rect = False if img_weight else rect
        self.rect = rect
        self.mosaic = self.aug and not self.rect
        self.mosaic_border = [-img_size // 2, -img_size // 2]
        self.stride = stride
        self.pad = pad

        self.label_files = [d.replace('Annotations', 'labels').replace('.xml', '.txt') for d in data]

        self.files = self.load_labels()
        labels, shapes = zip(*[self.files[x] for x in self.img_files])


        self.shapes = np.array(shapes, dtype=np.float64)
        self.labels = list(labels)

        # print(self.shapes)
        # print(self.labels)

        if self.rect:
            self.batch_shapes = rect_func(self.shapes, self.labels, self.img_files,
                                     self.label_files, nb, bi, self.img_size,
                                     self.stride, self.pad)

    def __len__(self) -> int:
        return len(self.img_files)

    def __getitem__(self, index: int):
        # if self.img_weights:
        #     index = self.indices[index]

        if self.mosaic:
            img, labels = mosaic(index, self.img_size, self.labels, self.img_files, self.aug)
            shapes = None

        else:
            img, (h0, w0), (h, w) = load_image(index, self.img_files, self.img_size, self.aug)

            shape = self.batch_shapes[self.bacth[index]] if self.rect else self.img_size

            img, ratio, pad = self.letterbox(img, shape, auto=False, scaleUp=self.aug)
            shapes = (h0, w0), ((h / h0, w / w0), pad)

            labels = []
            x = self.labels[index]
            if x.size > 0:
                labels = x.copy()
                labels[:, 1] = ratio[0] * w * (x[:, 1] - x[:, 3] / 2) + pad[0]
                labels[:, 2] = ratio[1] * h * (x[:, 2] - x[:, 4] / 2) + pad[1]
                labels[:, 3] = ratio[0] * w * (x[:, 1] + x[:, 3] / 2) + pad[0]
                labels[:, 4] = ratio[1] * h * (x[:, 2] + x[:, 4] / 2) + pad[1]


        # if self.aug:
        #     if not self.mosaic:

        nL = len(labels)
        if nL:
            labels[:, 1:5] = xyxy2xywh(labels[:, 1:5])

            labels[:, [2, 4]] /= img.shape[0]
            labels[:, [1, 3]] /= img.shape[1]


        labels_out = torch.zeros((nL, 6))
        if nL:
            labels_out[:, 1:] = torch.from_numpy(labels)

        img = img[:, :, ::-1].transpose(2, 0, 1)
        img = np.ascontiguousarray(img)

        return torch.from_numpy(img), labels_out, self.img_files[index], shapes

    @staticmethod
    def collate_fn(batch):
        img, label, path, shapes = zip(*batch)
        for i, l in enumerate(label):
            l[:, 0] = i

        return torch.stack(img, 0), torch.cat(label, 0), path, shapes



    def letterbox(self, img, new_shape=(416, 416), color=(114, 114, 114), auto=True, scaleFill=False, scaleUp=True):
        shape = img.shape[:2]
        if isinstance(new_shape, int):
            new_shape = (new_shape, new_shape)

        r = min(new_shape[0] / shape[0], new_shape[1] / shape[1])
        if not scaleUp:
            r = min(r, 1.0)

        ratio = (r, r)
        new_unpad = (int(round(shape[1] * r)), int(round(shape[0] * r)))
        dw, dh = new_shape[1] - new_unpad[0], new_shape[0] - new_unpad[1]

        if auto:
            dw, dh = np.mod(dw, 64), np.mod(dh, 64)
        elif scaleFill:
            dw, dh = 0., 0.
            new_unpad = (new_shape[1], new_shape[0])
            ratio = new_shape[1] / shape[1], new_shape[0] / shape[0]
        else:
            pass

        dw /= 2
        dh /= 2

        if shape[::-1] != new_unpad:
            img = cv2.resize(img, new_unpad, interpolation=cv2.INTER_LINEAR)

        top, bottom = int(round(dh - 0.1)), int(round(dh + 0.1))
        left, right = int(round(dw - 0.1)), int(round(dw + 0.1))
        img = cv2.copyMakeBorder(img, top, bottom, left, right, cv2.BORDER_CONSTANT, value=color)
        return img, ratio, (dw, dh)

    def load_labels(self):
        x = {}
        pbar = tqdm(zip(self.img_files, self.label_files), desc='Scanning Images', total=len(self.img_files))
        for (img, label) in pbar:
            try:
                l = []
                image = Image.open(img)
                image.verify()
                shape = image.size
                with open(label, 'r') as f:
                    l = np.array([x.split() for x in f.read().splitlines()], dtype=np.float32)
                if len(l) == 0:
                    l = np.zeros((0, 5), dtype=np.float32)
                x[img] = [l, shape]
            except Exception as e:
                x[img] = None
                print(e)
        return x

if __name__ == "__main__":
    data_path = '/data/line/voc/yolo.data'
    yolo_data = YOLODataset(data_path)
    # print(next(iter(yolo_data)))
    # next(iter(YOLODataset(data)))