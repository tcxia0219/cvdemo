# -*- coding: utf-8 -*-
'''
# Created on 1月-06-21 13:18
# @filename: model.py
# @author: tcxia
'''
from numpy.ma.core import masked_outside
import torch
import torch.nn as nn

import math

# from utils.layers import UnsampleFunc
from utils.layers import (
    MixConv2d, 
    Swish, 
    Mish, 
    DeformConv2d, 
    FeatureConcat, 
    WeightFeatureFusion, 
    YOLOLayer,
    UpsampleFunc)
# from utils.layers import *
from utils.parser_cfg import parse_model_cfg
from utils.util import scale_img


def create_modules(module_defs, img_size):
    img_size = [img_size] * 2 if isinstance(img_size, int) else img_size
    _ = module_defs.pop(0)
    output_filters = [3]
    module_list = nn.ModuleList()

    routs = []
    yolo_index = -1

    for i, mdef in enumerate(module_defs):
        modules = nn.Sequential()

        if mdef['type'] == 'convolutional':
            bn = mdef['batch_normalize']
            filters = mdef['filters']
            ks = mdef['size']
            stride = mdef['stride'] if 'stride' in mdef else (mdef['stride_y'], mdef['stride_x'])
            if isinstance(ks, int):
                modules.add_module('Conv2d', nn.Conv2d(
                    in_channels=output_filters[-1],
                    out_channels=filters,
                    kernel_size=ks,
                    stride=stride,
                    padding=ks//2 if mdef['pad'] else 0,
                    groups=mdef['groups'] if 'groups' in mdef else 1,
                    bias=not bn
                ))
            else:
                modules.add_module('MixConv2d', MixConv2d(
                    in_ch=output_filters[-1],
                    out_ch=filters,
                    k=ks,
                    stride=stride,
                    bias=not bn))

            if bn:
                modules.add_module('BatchNorm2d', nn.BatchNorm2d(filters, momentum=0.03, eps=1e-4))
            else:
                routs.append(i)

            if mdef['activation'] == 'leaky':
                modules.add_module('activation', nn.LeakyReLU(0.1, inplace=True))
            elif mdef['activation'] == 'swish':
                modules.add_module('activation', Swish())
            elif mdef['activation'] == 'mish':
                modules.add_module('activation', Mish())

        elif mdef['type'] == 'deformableconvolutional':
            bn = mdef['batch_normalize']
            filters = mdef['filters']
            ks = mdef['size']
            stride = mdef['stride'] if 'stride' in mdef else (mdef['stride_y'], mdef['stride_x'])
            if isinstance(ks, int):
                modules.add_module('DeformConv2d', DeformConv2d(
                    inc=output_filters[-1],
                    outc=filters,
                    kernel_size=ks,
                    padding=ks//2 if mdef['pad'] else 0,
                    stride=stride,
                    bias=not bn,
                    modulation=True
                ))
            else:
                modules.add_module('MixConv2d', MixConv2d(
                    in_ch=output_filters[-1],
                    out_ch=filters,
                    k=ks,
                    stride=stride,
                    bias=not bn
                ))

            if bn:
                modules.add_module('BatchNorm2d', nn.BatchNorm2d(filters, momentum=0.03, eps=1e-4))
            else:
                routs.append(i)

            if mdef['activation'] == 'leaky':
                modules.add_module('activation', nn.LeakyReLU(0.1, inplace=True))
            elif mdef['activation'] == 'swish':
                modules.add_module('activation', Swish())
            elif mdef['activation'] == 'mish':
                modules.add_module('activation', Mish())

        elif mdef['type'] == 'BatchNorm2d':
            filters = output_filters[-1]
            modules = nn.BatchNorm2d(filters, momentum=0.03, eps=1e-4)
            if i == 0 and filters == 3:
                modules.running_mean = torch.tensor([0.485, 0.456, 0.406])
                modules.running_var = torch.tensor([0.0524, 0.0502, 0.0506])

        elif mdef['type'] == 'maxpool':
            ks = mdef['size']
            stride = mdef['stride']
            maxpool = nn.MaxPool2d(kernel_size=ks, stride=stride, padding=(ks - 1) // 2)
            if ks == 2 and stride == 1:
                modules.add_module('ZeroPad2d', nn.ZeroPad2d((0, 1, 0, 1)))
                modules.add_module('MaxPool2d', maxpool)
            else:
                modules = maxpool

        elif mdef['type'] == 'upsample':
            modules = UpsampleFunc(scale_factor=mdef['stride'])

        elif mdef['type'] == 'route':
            layers = mdef['layers']
            filters = sum([output_filters[l + 1 if l > 0 else l] for l in layers])
            routs.extend([i + l if l < 0 else l for l in layers])
            modules = FeatureConcat(layers=layers)

        elif mdef['type'] == 'shortcut':
            layers = mdef['from']
            filters = output_filters[-1]
            # routs.extend([i + l if l < 0 else l for l in layers])
            routs.append(i + layers[0])
            modules = WeightFeatureFusion(layers=layers, weight='weights_type' in mdef)

        elif mdef['type'] == 'yolo':
            yolo_index += 1
            # stride = [8, 16, 32, 64, 128]
            stride = [32, 16, 8]

            layers = mdef['from'] if 'from' in mdef else []

            modules = YOLOLayer(
                anchors=mdef['anchors'][mdef['mask']],
                nc=mdef['classes'],
                img_size=img_size,
                # yolo_index=yolo_index,
                # layers=layers,
                stride=stride[yolo_index]
            )

            try:
                # j = layers[yolo_index] if 'from' in mdef else -1
                j = -1
                bias_ = module_list[j][0].bias
                # bias = bias_[:modules.no * modules.na].view(modules.na, -1)
                bias = bias_.view(modules.na, -1)

                # bias[:, 4] += math.log(8 / (640 / stride[yolo_index]) ** 2)
                bias[:, 4] += -4.5
                bias[:, 5:] += math.log(0.6 / (modules.nc - 0.99))

                module_list[j][0].bias = torch.nn.Parameter(bias_, requires_grad=bias_.requires_grad)
            except:
                print("smart bias init failure")
        else:
            print("Unrecognized Layer Type:", mdef['type'])

        module_list.append(modules)
        output_filters.append(filters)

    routs_bin = [False] * (len(module_defs))
    for i in routs:
        routs_bin[i] = True
    return module_list, routs_bin


def get_yolo_layer(model):
    return [i for i, m in enumerate(model.module_list) if m.__class__.__name__ == 'YOLOLayer']


class DarkNet(nn.Module):
    def __init__(self, cfg, img_size=(416, 416), verbose=False) -> None:
        super().__init__()


        self.module_defs = parse_model_cfg(cfg)
        self.module_list, self.routs = create_modules(self.module_defs, img_size)
        self.yolo_layers = get_yolo_layer(self)


    def forward(self, x):
        # if not aug:
        #     return self.forward_once(x)
        # else:
        #     img_size = x.shape[-2:]
        #     s = [0.83, 0.67]
        #     y = []
        #     for i, xi in enumerate((
        #         x,
        #         scale_img(x.flip(3), s[0], same_shape=False),
        #         scale_img(x, s[1], same_shape=False)
        #     )):
        #         y.append(self.forward_once(xi)[0])

        #     y[1][..., :4] /= s[0]
        #     y[1][..., 0] = img_size[1] - y[1][..., 0]
        #     y[2][..., :4] /= s[1]
        #     y = torch.cat(y, 1)
        #     return y, None
        return self.forward_once(x)


    def forward_once(self, x):
        # img_size = x.shape[-2:]
        yolo_out, out = [], []

        # if aug:
        #     nb = x.shape[0]
        #     s = [0.83, 0.67]
        #     x = torch.cat((
        #         x,
        #         scale_img(x.flip(3), s[0]),
        #         scale_img(x, s[1]),
        #     ), 0)

        for i, module in enumerate(self.module_list):
            name = module.__class__.__name__
            # print(name)
            if name in ['WeightFeatureFusion', 'FeatureConcat']:
                x = module(x, out)
            elif name == 'YOLOLayer':
                yolo_out.append(module(x, out))
            else:
                x = module(x)

            out.append(x if self.routs[i] else [])

        # print(self.training)
        if self.training:
            return yolo_out
        else:
            x, p = zip(*yolo_out)
            x = torch.cat(x, 1)
            return x, p
