# -*- coding: utf-8 -*-
'''
# Created on 1月-06-21 12:59
# @filename: train.py
# @author: tcxia
'''

import random
import yaml
import math
import numpy as np

import torch
import torch.utils.data as tud
import torch.nn.functional as F

from utils.util import compute_loss, check_img_size, nms, clip_coords, ap_per_class, xywh2xyxy, box_iou
from data.datasets import YOLODataset
from models.model import DarkNet

pretrained_flag = False
batch_size = 4
epoches = 20
accumulate = max(round(64 / batch_size), 1)
img_size = (416, 416)
gs = 32


device = torch.device("cuda:3" if torch.cuda.is_available() else "cpu")

def Trainer(model, optimizer, train_loader, device, epoches, accumulate, lf, scheduler, gs=32, imgsz=416, multi_scale=True, save=False):
    ckpt_dict = {}
    for epoch in range(epoches):
        model.train()
        nb = len(train_loader)
        nw = max(3 * nb, 1e3)

        optimizer.zero_grad()
        for i, (imgs, targets, paths, _) in enumerate(train_loader):
            ni = i + nb * epoch

            imgs = imgs.to(device).float() / 255.0
            targets = targets.to(device)

            if ni <= nw:
                xi = [0, nw]
                accumulate = max(1, np.interp(ni, xi, [1, accumulate]).round())
                for j, x in enumerate(optimizer.param_groups):
                    x['lr'] = np.interp(ni, xi, [0.1 if j == 2 else 0., x['initial_lr'] * lf(epoch)])
                    if 'momentum' in x:
                        x['momentum'] = np.interp(ni, xi, [0.9, hyp['momentum']])


            # if multi_scale:
            #     grid_min, grid_max = img_size // gs, img_size // gs
            #     _, _, new_grid_min, new_grid_max = build_multi_scale(img_size, grid_min, grid_max, gs)
            #     if ni % accumulate == 0:
            #         img_size = random.randrange(new_grid_min, new_grid_max + 1) * gs
            #     sf = img_size / max(imgs.shape[2:])
            #     if sf != 1:
            #         ns = [math.ceil(x * sf / gs) * gs for x in imgs.shape[2:]]
            #         imgs = F.interpolate(imgs, size=ns, mode='bilinear', align_corners=False)

            if multi_scale:
                sz = random.randrange(imgsz * 0.5, imgsz * 1.5 + gs) // gs * gs
                sf = sz / max(imgs.shape[2:])
                # print(sf)
                if sf != 1:
                    ns = [math.ceil(x * sf / gs) * gs for x in imgs.shape[2:]]
                    imgs = F.interpolate(imgs, size=ns, mode='bilinear', align_corners=False)


            pred = model(imgs)
            loss, _ = compute_loss(pred, targets, model)
            # losses = sum(loss for loss in loss_dict.values())

            if ni % accumulate == 0:
                loss.backward()
                optimizer.step()

            print("Epoch: {} | Iter: {} | loss: {}".format(epoch, i, loss.item()))

        scheduler.step()

        if (epoch+1) % 5 == 0:
            print('===== Evalution =====')
            ckpt = {
                'epoch': epoch,
                'model': model.state_dict(),
            }
            torch.save(ckpt, f'checkpoints/model.pth')

            Evaler(model, train_loader, imgsz, device)


def Evaler(model, dev_loader, imgsz, device):
    model.eval()

    img = torch.zeros((1, 3, imgsz, imgsz), device=device)
    _ = model(img)

    # names =  model.names if hasattr(model, 'names') else model.module.names
    p, r, f1, mp, mr, map50, map50_95 = 0., 0., 0., 0., 0., 0., 0.

    jdict, stats, ap, ap_class = [], [], [], []

    iouv = torch.linspace(0.5, 0.95, 10).to(device)
    niou = iouv.numel()

    for i, (imgs, targets, paths, _) in enumerate(dev_loader):
        imgs = imgs.to(device)
        imgs = imgs.to(device).float() / 255.0

        targets = targets.to(device)
        nb, _, h, w = imgs.shape
        whwh = torch.Tensor([w, h, w, h]).to(device)

        with torch.no_grad():
            inf_out, train_out = model(imgs)
            # print(inf_out.shape)
            output = nms(inf_out)

        for si, pred in enumerate(output):
            labels = targets[targets[:, 0] == si, 1:]
            nl = len(labels)
            tcls = labels[:, 0].tolist() if nl else []

            clip_coords(pred, (h, w))

            correct = torch.zeros(pred.shape[0], niou, dtype=torch.bool, device=device)

            if nl:
                detected = []
                tcls_tensor = labels[:, 0]

                tbox = xywh2xyxy(labels[:, 1:5]) * whwh

                for _cls in torch.unique(tcls_tensor):
                    ti = (_cls == tcls_tensor).nonzero(as_tuple=False).view(-1)
                    pi = (_cls == pred[:, 5]).nonzero(as_tuple=False).view(-1)

                    if pi.shape[0]:
                        ious, i = box_iou(pred[pi, :4], tbox[ti]).max(1)

                        for j in (ious > iouv[0]).nonzero(as_tuple=False):
                            d = ti[i[j]]
                            if d not in detected:
                                detected.append(d)
                                correct[pi[j]] = ious[j] > iouv
                                if len(detected) == nl:
                                    break
            stats.append((correct.cpu(), pred[:, 4].cpu(), pred[:, 5].cpu(), tcls))

    stats = [np.concatenate(x, 0) for x in zip(*stats)]
    if len(stats) and stats[0].any():
        p, r, ap, f1, ap_class = ap_per_class(*stats)
        p, r, ap50, ap = p[:, 0], r[:, 0], ap[:, 0], ap.mean(1)
        mp, mr, map50, map50_95 = p.mean(), r.mean(), ap50.mean(), ap.mean()

    print(mp, mr, map50, map50_95)



def load_pretrained(cfg, weights_path):
    ckpt = torch.load(weights_path, map_location=device)
    model = DarkNet(cfg).to(device)
    state_dict = {k:v for k, v in ckpt['model'].items() if model.state_dict()[k].numel() == v.numel()}
    model.load_state_dict(state_dict, strict=False)
    return model


def build_optim(model, lr, momentum, weight_decay, adam=True):
    pg0, pg1, pg2 = [], [], []

    for k, v in dict(model.named_parameters()).items():
        if '.bias' in k:
            pg2.append(v)
        elif 'Conv2d.weight' in k:
            pg1.append(v)
        else:
            pg0.append(v)

    if adam:
        optimizer = torch.optim.Adam(pg0, lr=lr, betas=(momentum, 0.999))
    else:
        optimizer = torch.optim.SGD(pg0, lr=lr, momentum=momentum, nesterov=True)

    optimizer.add_param_group({'params': pg1, 'weight_decay': weight_decay})
    optimizer.add_param_group({'params': pg2})

    del pg0, pg1, pg2
    return optimizer

def build_multi_scale(img_size, grid_min, grid_max, gs):

    imgsz_min = img_size // 1.5
    imgsz_max = img_size // 0.667

    grid_min, grid_max = imgsz_min // gs, imgsz_max // gs

    imgsz_min, imgsz_max = int(grid_min * gs), int(grid_max * gs)

    return imgsz_min, imgsz_max, grid_min, grid_max


if __name__ == "__main__":
    file_path = '/data/line/voc/yolo.data'
    train_set = YOLODataset(file_path)
    train_dataloader = tud.DataLoader(train_set,
                                      batch_size=2,
                                      shuffle=True,
                                      pin_memory=True,
                                      num_workers=0,
                                      collate_fn=train_set.collate_fn)

    cfg_file = 'configs/yolov3-spp.cfg'
    weights_path = 'checkpoints/model.pth'
    if pretrained_flag:
        model = load_pretrained(cfg_file, weights_path)
    else:
        model = DarkNet(cfg_file).to(device)
    # print(model.named_parameters)
    hyp_file = 'configs/hyper_params.yaml'
    with open(hyp_file) as f:
        hyp = yaml.load(f, Loader=yaml.FullLoader)

    optimizer = build_optim(model, hyp['lr0'], hyp['momentum'], hyp['weight_decay'])

    lf = lambda x: (((1 + math.cos(x * math.pi / epoches)) / 2) ** 1.0) * 0.8 + 0.2
    scheduler = torch.optim.lr_scheduler.LambdaLR(optimizer, lr_lambda=lf)

    nc, names = int(hyp['nc']), hyp['names']
    model.hyp = hyp
    model.gr = 1.0
    model.nc = nc
    model.name = names

    imgsz, _ = [check_img_size(x, gs) for x in img_size]

    Trainer(model,
            optimizer,
            train_dataloader,
            device,
            epoches,
            accumulate,
            imgsz=imgsz,
            lf=lf,
            scheduler=scheduler)



    # Evaler(model, train_dataloader, imgsz, device)
