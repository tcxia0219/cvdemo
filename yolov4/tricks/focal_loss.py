# -*- coding: utf-8 -*-
'''
# Created on 2021/01/07 12:36:26
# @filename: focal_loss.py
# @author: tcxia
'''

import torch
import torch.nn as nn

class FocalLoss(nn.Module):
    def __init__(self, loss_fcn, gamma=1.5, alpha=0.25) -> None:
        super().__init__()

        self.loss_fcn = loss_fcn
        self.gamma = gamma
        self.alpha = alpha

        self.reduction = loss_fcn.reduction
        self.loss_fcn.reduction = 'none'


    def forward(self, pred, true):
        loss = self.loss_fcn(pred, true)

        pred_prob = torch.sigmoid(pred)
        p_t = true * pred_prob + (1 - true) * (1 - pred_prob)
        alpha_factor = true * self.alpha + (1 - true) * (1 - self.alpha)
        modulating_factor = (1.0 - p_t) ** self.gamma
        loss *= alpha_factor * modulating_factor

        if self.reduction == 'mean':
            return loss.mean()

        elif self.reduction == 'sum':
            return loss.sum()

        else:
            return loss