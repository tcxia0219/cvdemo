# -*- coding: utf-8 -*-
'''
# Created on 2021/01/06 18:07:17
# @filename: mosaic.py
# @author: tcxia
'''
import random
import cv2
import numpy as np


def load_image(index, img_files, img_size, aug=False):
    img_path = img_files[index]
    img = cv2.imread(img_path)
    h0, w0 = img.shape[:2]
    r = img_size / max(h0, w0)
    if r != 1:
        interp = cv2.INTER_AREA if r < 1 and not aug else cv2.INTER_LINEAR
        img = cv2.resize(img, (int(w0 * r), int(h0 * r)), interpolation=interp)
    return img, (h0, w0), img.shape[:2]


def mosaic(index, img_size, labels, img_files, aug=False):
    labels_4 = []
    s = img_size
    yc, xc = img_size, img_size
    indices = [index] + [random.randint(0, len(labels) - 1) for _ in range(3)]

    for i, idx in enumerate(indices):
        img, _, (h, w) = load_image(idx, img_files, img_size, aug)
        img_4 = np.full((s * 2, s * 2, img.shape[2]), 114, dtype=np.uint8)
        if i == 0:
            x1a, y1a, x2a, y2a = max(xc - w, 0), max(yc - h, 0), xc, yc
            x1b, y1b, x2b, y2b = w - (x2a - x1a), h - (y2a - y1a), w, h
        elif i == 1:
            x1a, y1a, x2a, y2a = xc, max(yc - h, 0), min(xc + w, s * 2), yc
            x1b, y1b, x2b, y2b = 0, h - (y2a - y1a), min(w, x2a - x1a), h
        elif i == 2:
            x1a, y1a, x2a, y2a = max(xc - w, 0), yc, xc, min(s * 2, yc + h)
            x1b, y1b, x2b, y2b = w - (x2a - x1a), 0, max(xc, w), min(y2a - y1a, h)
        else:
            x1a, y1a, x2a, y2a = xc, yc, min(xc + w, s * 2), min(s * 2, yc + h)
            x1b, y1b, x2b, y2b = 0, 0, min(w, x2a - x1a), min(y2a - y1a, h)

        img_4[y1a:y2a, x1a:x2a] = img[y1b:y2b, x1b:x2b]
        padw = x1a - x1b
        padh = y1a - y1b

        x = labels[idx]
        labels = x.copy()
        if x.size > 0:
            labels[:, 1] = w * (x[:, 1] - x[:, 3] / 2) + padw
            labels[:, 2] = h * (x[:, 2] - x[:, 4] / 2) + padh
            labels[:, 3] = w * (x[:, 1] + x[:, 3] / 2) + padw
            labels[:, 4] = h * (x[:, 2] + x[:, 4] / 2) + padh
        labels_4.append(labels)

    if len(labels_4) > 0:
        labels_4 = np.concatenate(labels_4, 0)
        np.clip(labels_4[:, 1:], 0, 2 * s, out=labels_4[:, 1:])

    # img_4, labels_4 =
    return img_4, labels_4
