# -*- coding: utf-8 -*-
'''
# Created on 2021/01/06 17:48:55
# @filename: rect.py
# @author: tcxia
'''

import numpy as np

def rect_func(shapes, labels, img_files, label_files, num_batch, batch_index, img_size, stride, pad):
    ar = shapes[:, 1] / s[:, 0]
    irect = ar.argsort()

    img_files = [img_files[i] for i in irect]
    label_files = [label_files[i] for i in irect]

    labels = [labels[i] for i in irect]
    shapes = shapes[irect]
    ar = ar[irect]

    new_shapes = [[1, 1]] * num_batch
    for i in range(num_batch):
        ari = ar[batch_index == i]
        mini, maxi = ari.min(), ari.max()

        if maxi < 1:
            new_shapes[i] = [maxi, 1]
        elif mini > 1:
            new_shapes[i] = [1, 1 / mini]
        else:
            pass
    batch_shapes = np.ceil(np.array(new_shapes) * img_size / stride + pad).astype(np.int) * stride
    return batch_shapes
