# -*- coding: utf-8 -*-
'''
# Created on 1月-06-21 15:43
# @filename: util.py
# @author: tcxia
'''
import math

import torch
import torch.nn as nn
import torch.nn.functional as F
import torchvision
import numpy as np

from tricks.focal_loss import FocalLoss
from tricks.ious import bbox_iou


def scale_img(img, ratio=1.0, same_shape=False):
    if ratio == 1.0:
        return img
    else:
        h, w = img.shape[2:]
        s = (int(h * ratio), int(w * ratio))
        img = F.interpolate(img, size=s, mode='bilinear', align_corners=False)
        if not same_shape:
            gs = 32
            h, w = [math.ceil(x * ratio / gs) * gs for x in (h, w)]
        return F.pad(img, [0, w  - s[1], 0, h - s[0]], value=0.447)


def compute_loss(preds, targets, model):
    device = targets.device

    lcls, lbox, lobj = torch.zeros(1, device=device), torch.zeros(1, device=device), torch.zeros(1, device=device)
    tcls, tbox, indices, anchors = build_targets(preds, targets, model)
    h = model.hyp

    BCEcls = nn.BCEWithLogitsLoss(pos_weight=torch.Tensor([h['cls_pw']])).to(device)
    BCEobj = nn.BCEWithLogitsLoss(pos_weight=torch.Tensor([h['obj_pw']])).to(device)

    cp, cn = smooth_BCE(eps=0.)

    g = h['fl_gamma']
    if g > 0:
        BCEcls, BCEobj = FocalLoss(BCEcls, g), FocalLoss(BCEobj, g)

    nt = 0
    np = len(preds)
    balance = [4., 1., 0.4] if np==3 else [4., 1., 0.4, 0.1]

    for i, pi in enumerate(preds):
        b, a, gj, gi = indices[i]
        tobj = torch.zeros_like(pi[..., 0], device=device)

        nb = b.shape[0]
        if nb:
            nt += nb
            ps = pi[b, a, gj, gi]

            pxy = ps[:, :2].sigmoid() * 2. - 0.5
            pwh = (ps[:, 2:4].sigmoid() * 2) ** 2 * anchors[i]
            # pxy = ps[:, :2].sigmoid()
            # pwh = ps[:, 2:4].clamp(max=1e3) * anchors[i]

            pbox = torch.cat((pxy, pwh), 1).to(device)
            giou = bbox_iou(pbox.T, tbox[i], x1y1x2y2=False, GIoU=True)
            lbox += (1.0 - giou).mean()

            tobj[b, a, gj, gi] = (1.0 - model.gr) + model.gr * giou.detach().clamp(0).type(tobj.dtype)

            if model.nc > 1:
                t = torch.full_like(ps[:, 5:], cn, device=device)
                t[range(nb), tcls[i]] = cp
                lcls += BCEcls(ps[:, 5:], t)

        lobj += BCEobj(pi[..., 4], tobj) * balance[i]
        # lobj += BCEobj(pi[..., 4], tobj)

    s = 3 / np
    lbox *= h['giou'] * s
    lobj *= h['obj'] * s * (1.4 if np==4 else 1.)
    lcls *= h['cls'] * s
    bs = tobj.shape[0]

    loss = lbox + lobj + lcls
    # lbox *= h['giou']
    # lobj *= h['obj']
    # lcls *= h['cls']
    return loss * bs, torch.cat((lbox, lobj, lcls, loss)).detach()
    # return {'box_loss': lbox, 'obj_loss': lobj, 'cls_loss': lcls}


def smooth_BCE(eps=0.1):
    return 1.0 - 0.5 * eps, 0.5 * eps

def build_targets(preds, targets, model):
    nt = targets.shape[0]
    tcls, tbox, indices, anchors = [], [], [], []
    gain = torch.ones(6, device=targets.device)
    off = torch.tensor([[1, 0], [0, 1], [-1, 0], [0, -1]], device=targets.device)

    g = 0.5
    for i, yl in enumerate(model.yolo_layers):
        anchor = model.module_list[yl].anchor_vec
        gain[2:] = torch.tensor(preds[i].shape)[[3, 2, 3, 2]]

        a, t, offsets = [], targets * gain, 0
        if nt:
            na = anchor.shape[0]
            at = torch.arange(na).view(na, 1).repeat(1, nt)
            r = t[None, :, 4:6] / anchor[:, None]
            j = torch.max(r, 1. / r).max(2)[0] < model.hyp['anchor_t']
            a, t = at[j], t.repeat(na, 1, 1)[j]

            gxy = t[:, 2:4]
            z = torch.zeros_like(gxy)
            j, k = ((gxy % 1. < g) & (gxy > 1.)).T
            l, m = ((gxy % 1. > (1 - g)) & (gxy < (gain[[2, 3]] - 1.))).T
            a, t = torch.cat((a, a[j], a[k], a[l], a[m]), 0), torch.cat((t, t[j], t[k], t[l], t[m]), 0)
            offsets = torch.cat((z, z[j] + off[0], z[k] + off[1], z[l] + off[2], z[m] + off[3]), 0) * g

        b, c = t[:, :2].long().T
        gxy = t[:, 2:4]
        gwh = t[:, 4:6]
        gij = (gxy - offsets).long()
        gi, gj = gij.T

        indices.append((b, a, gj.clamp_(0, gain[3] - 1), gi.clamp_(0, gain[2] - 1)))
        tbox.append(torch.cat((gxy - gij, gwh), 1))
        anchors.append(anchor[a])
        tcls.append(c)

    return tcls, tbox, indices, anchors


def xyxy2xywh(x):
    y = torch.zeros_like(x) if isinstance(x, torch.Tensor) else np.zeros_like(x)
    y[:, 0] = (x[:, 0] + x[:, 2]) / 2
    y[:, 1] = (x[:, 1] + x[:, 3]) / 2
    y[:, 2] = x[:, 2] - x[:, 0]
    y[:, 3] = x[:, 3] - x[:, 1]
    return y

def xywh2xyxy(x):
    y = torch.zeros_like(x) if isinstance(x, torch.Tensor) else np.zeros_like(x)
    y[:, 0] = x[:, 0] - x[:, 2] / 2
    y[:, 1] = x[:, 1] - x[:, 3] / 2
    y[:, 2] = x[:, 0] + x[:, 2] / 2
    y[:, 3] = x[:, 1] + x[:, 3] / 2
    return y

def make_divisiable(x, divisor):
    return math.ceil(x / divisor) * divisor

def check_img_size(img_size, s=32):
    new_size = make_divisiable(img_size, int(s))
    if new_size != img_size:
        print("img size must be multiple of max stride")
    return new_size


def nms(pred, conf_thres=0.01, iou_thres=0.6):
    
    nc = pred[0].shape[1] - 5
    print(pred[..., 4])
    xc = pred[..., 4] > conf_thres
    print(xc)

    min_wh, max_wh = 2, 4096
    max_det = 300

    output = [None] * pred.shape[0]
    for xi, x in enumerate(pred):
        x = x[xc[xi]]

        if not x.shape[0]:
            continue
        
        x[:, 5:] *= x[:, 4:5]
        box = xywh2xyxy(x[:, :4])

        conf, j = x[:, 5:].max(1, keepdim=True)
        x = torch.cat((box, conf, j.float()), 1)[conf.view(-1) > conf_thres]

        n = x.shape[0]
        if not n:
            continue

        c = x[:, 5:6] * max_wh
        boxes, scores = x[:, :4] + c, x[:, 4]
        i = torchvision.ops.nms(boxes, scores, iou_thres)
        if i.shape[0] > max_det:
            i = i[:max_det]
        
        output[xi] = x[i]
    return output


def clip_coords(boxes, img_shape):
    boxes[:, 0].clamp_(0, img_shape[1])
    boxes[:, 1].clamp_(0, img_shape[0])
    boxes[:, 2].clamp_(0, img_shape[1])
    boxes[:, 3].clamp_(0, img_shape[0])


def ap_per_class(tp, conf, pred_cls, target_cls):
    i = np.argsort(-conf)
    tp, conf, pred_cls = tp[i], conf[i], pred_cls[i]

    unique_cls = np.unique(target_cls)

    pr_socre = 0.1
    s = [unique_cls.shape[0], tp.shape[1]]
    ap, p, r = np.zeros(s), np.zeros(s), np.zeros(s)

    for ci, c in enumerate(unique_cls):
        i = pred_cls==c
        n_gt = (target_cls == c).sum()
        n_p = i.sum()

        if n_p == 0 or n_gt == 0:
            continue
        else:
            fpc = (1 - tp[i]).cumsum(0)
            tpc = tp[i].cumsum(0)

            recall = tpc / (n_gt + 1e-16)
            r[ci] = np.interp(-pr_socre, -conf[i], recall[:, 0])

            precision = tpc / (tpc + fpc)
            p[ci] = np.interp(-pr_socre, -conf[i], precision[:, 0])

            for j in range(tp.shape[1]):
                ap[ci, j] = compute_ap(recall[:, j], precision[:, j])
    f1 = 2 * p * r / (p + r + 1e-16)
    return p, r, ap, f1, unique_cls.astype('int32')

def compute_ap(recall, precision):
    mrec = np.concatenate(([0.], recall, [min(recall[-1] + 1e-3, 1.)]))
    mpre = np.concatenate(([0.], precision, [0.]))

    mpre = np.flip(np.maximum.accumulate(np.flip(mpre)))

    method = 'interp'
    if method == 'interp':
        x = np.linspace(0, 1, 101)
        ap = np.trapz(np.interp(x, mrec, mpre), x)
    else:
        i = np.where(mrec[1:] != mrec[:-1])[0]
        ap = np.sum((mrec[i + 1] - mrec[i]) * mpre[i + 1])
    return ap


def box_iou(box1, box2):
    
    def box_area(box):
        return (box[2] - box[0]) * (box[3] - box[1])

    area1 = box_area(box1.T)
    area2 = box_area(box2.T)

    inter = (torch.min(box1[:, None, 2:], box2[:, 2:]) - torch.max(box1[:, None, :2], box2[:, :2])).clamp(0).prod(2)
    return inter / (area1[:, None] + area2 - inter)